import re

class DummyMatchesHTML:
    def __init__(self, t1, t2, gameid=123456):
        content = f"window.espn.scoreboardData"
        content += f"\"homeAway\":\"home\" \"team\":{{ \"alternateColor\":colour \"displayName\":\"{t1}\" "
        content += f"\"homeAway\":\"away\" \"team\":{{ \"alternateColor\":colour \"displayName\":\"{t2}\" "
        content += f"\"href\":\"http://www.espn.com/soccer/matchstats?gameId={gameid}\","
        content += f"\"text\":\"Statistics\"<body class=\"scoreboard\""
        self.text = content

    def add_game(self, t1, t2, gameid=111111):
        content = f"window.espn.scoreboardData"
        content += f"\"homeAway\":\"home\" \"team\":{{ \"alternateColor\":colour \"displayName\":\"{t1}\" "
        content += f"\"homeAway\":\"away\" \"team\":{{ \"alternateColor\":colour \"displayName\":\"{t2}\" "
        content += f"\"href\":\"http://www.espn.com/soccer/matchstats?gameId={gameid}\","
        content += f"\"text\":\"Statistics\""
        self.text = self.text.split("window.espn.scoreboardData")[1]
        self.text = content + self.text



    def as_string(self):
        return bytes(self.text, 'utf-8')


class DummyGameHTML:
    def __init__(self, provided_info="content", exists=True):
        content = ""
        if exists:
            content = f"data-stat=\"note\">{provided_info}<"
        self.text = content


    def as_string(self):
        return bytes(self.text, 'utf-8')


class DummySidebarHTML:

    def __init__(self, subs=["test1", "test2"], header=True):
        content = "<table>"
        if header:
            content += "<tr>\n#\nTeam\nPl\nGD\nPt\n</tr>"

        c = 0
        for s in subs:
            content += f"<tr>\nposition\n<a href=https://www.reddit.com/r/{s}/>team{c}</a>\nplayed\ngoaldiff\npoints\n</tr>"
            c += 1

        content += "</table>"
        self.text = content

    def clear(self):
        self.text = ""

    def replace_header(self, new_header):
        new_text = self.text.replace("<tr>\n#\nTeam\nPl\nGD\nPt\n</tr>", f"<tr>{new_header}</tr>")
        self.text = new_text


    def as_string(self):
        return bytes(self.text, 'utf-8')


class DummyBBCScorePage:
    def __init__(self, games):
        content = ""
        for game_id in games.keys():
            h, a = games[game_id]
            content += "<div class=\"qa-match-block\">"
            content += f"<li><a href=\"/sport/football/{game_id}\">"
            content += f"<span class=\"sp-c-fixture__team-name-wrap\"><span>{h}</span></span> "
            content += f"<span class=\"sp-c-fixture__team-name-wrap\"><span>{a}</span></span> "
            content += "</a></li></div>"

        self.text = content

    def add_link(self, match_id, new_link):
        self.text = self.text.replace(f"<a href=\"/sport/football/{match_id}\">",
                                      f"<a href=\"/sport/football/{match_id}\">"
                                      f"<a href=\"{new_link}\"></a>")

    def remove_team(self, team):
        self.text = self.text.replace(f"<span class=\"sp-c-fixture__team-name-wrap\"><span>{team}</span></span>",
                                      f"")

    def remove_team_span(self, team):
        self.text = self.text.replace(f"<span class=\"sp-c-fixture__team-name-wrap\"><span>{team}</span></span>",
                                      f"<span class=\"sp-c-fixture__team-name-wrap\">{team}</span>")

    def remove_teams_span(self):
        while "<span class=\"sp-c-fixture__team-name-wrap\">" in self.text:
            self.text = self.text.replace(f"<span class=\"sp-c-fixture__team-name-wrap\">",
                                          f"")


    def add_game_status(self, gameid, status):
        content = ""
        div_class = "<div class=\"qa-match-block\">"
        games = self.text.split(div_class)
        games = [g for g in games if g.strip() != '']
        for game in games:
            if gameid in game:
                info = game[:-15] + status + game[-15:]
            else:
                info = game
            content += div_class + info

        self.text = content

    def remove_link(self, match_id):
        self.text = self.text.replace(f"<a href=\"/sport/football/{match_id}\">",
                                      f"")


    def as_string(self):
        return bytes(self.text, 'utf-8')


class DummyBBCStatsPage:
    def __init__(self, possession, shots, shots_on_target, corners, fouls, status_code=200):
        p_a, p_b = possession
        s_a, s_b = shots
        st_a, st_b = shots_on_target
        c_a, c_b = corners
        f_a, f_b = fouls
        content = "<div class=\"sp-c-football-match-stats\"><dl>"
        content += f"<dl><dt>Possession</dt> {p_a} {p_b} </dl>"
        content += f"<dl><dt>Shots</dt> {s_a} {s_b} </dl>"
        content += f"<dl><dt>Shots on Target</dt> {st_a} {st_b} </dl>"
        content += f"<dl><dt>Corners</dt> {c_a} {c_b} </dl>"
        content += f"<dl><dt>Fouls</dt> {f_a} {f_b} </dl>"
        content += "</dl>"

        self.text = content
        self.status_code = status_code
        self.count = 0

    def remove_row(self, row_name):
        content = self.text
        new_content = ""
        rows = content.split("<dl>")
        done = False
        for row in rows:
            if row_name.lower() not in row.lower() or done:
                new_content += row + "<dl>"
            else:
                done = True

        if not new_content.endswith("</dl>"):
            # last row was removed, get rid of trailing <dl> and add a </dl> for the whole table
            new_content = new_content[:-4]+"</dl>"
        self.text = new_content

    def as_string(self):
        return bytes(self.text, 'utf-8')


class DummyTVHTML:

    def __init__(self, games):
        content = ""

        for teams in games.keys():
            tv_channel, day, ko_time, comp = games[teams]
            content += f"<div class=\"fixture-date\">{day}</div>"
            content += f"</div>"
            content += f"<div class=\"fixture\">"
            content += f"<div class=\"fixture__time\">{ko_time}</div>"
            content += f"<div class=\"fixture__teams\">{teams}</div>"
            content += f"<div class=\"fixture__competition\">{comp}</div>"
            content += f"<div class=\"fixture__channel\"><span class=\"channel-pill\">{tv_channel}</span></div>"
            content += f"</div></div>"


        self.text = content

    def add_games(self, games):
        content = ""

        for teams in games.keys():
            tv_channel, day, ko_time, comp = games[teams]
            content += f"<div class=\"fixture-date\">{day}</div>"
            content += f"</div>"
            content += f"<div class=\"fixture\">"
            content += f"<div class=\"fixture__time\">{ko_time}</div>"
            content += f"<div class=\"fixture__teams\">{teams}</div>"
            content += f"<div class=\"fixture__competition\">{comp}</div>"
            content += f"<div class=\"fixture__channel\"><span class=\"channel-pill\">{tv_channel}</span></div>"
            content += f"</div></div>"


        self.text = self.text[:-6] + content

    def remove_div_class(self, div_class):
        objs = re.split(f"<div class=\"{div_class}\">(?:[^<]*)</div>", self.text)
        content = ""
        for c in objs:
            content += c

        self.text = content


class DummyGamePage:
    def __init__(self, events):
        content = "stuff<h1>Match Commentary</h1>stuff<h1>Key Events</h1>"
        for event in events:
            content += f"<tr data-id=({event})</tr>"

        self.text = content