import sys
import pathlib
topdir = pathlib.Path(__file__).absolute().parent.parent.as_posix()
sys.path.append(topdir)
import unittest
import os

from tests.scraper.find_bbc_page_tests import *
from tests.scraper.game_info_tests import *
from tests.scraper.game_stats_tests import *
from tests.scraper.get_team_info_tests import *
from tests.scraper.get_times_tests import *
from tests.scraper.grab_bbc_stats_tests import *
from tests.scraper.grab_tv_tests import *
from tests.scraper.guess_right_match_tests import *
from tests.scraper.match_finder_tests import *
from tests.scraper.team_info_tests import *




if __name__ == '__main__':
    print("Scraper Tests")
    os.chdir(topdir + "/tests")
    os.makedirs(os.path.join(os.getcwd(), "logs"))
    # print(os.getcwd())
    unittest.main()
    os.rmdir(os.path.join(os.getcwd(), "logs"))
