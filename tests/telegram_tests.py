import sys
import pathlib
topdir = pathlib.Path(__file__).absolute().parent.parent.as_posix()
sys.path.append(topdir)
import unittest
from tests.telegram.access_tests import *
import os

if __name__ == '__main__':
    print("Telegram Tests")
    os.chdir(topdir + "/tests")
    os.makedirs(os.path.join(os.getcwd(), "logs"))
    # print(os.getcwd())
    unittest.main()
    os.rmdir(os.path.join(os.getcwd(), "logs"))
