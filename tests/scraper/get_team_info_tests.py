from unittest import TestCase
from unittest.mock import patch, mock_open, Mock
import sys
import pathlib
import os
import requests

from src.scraper import Scraper
from tests.DummyClasses import *
from src.codes import *

class GetTeamInfoTests(TestCase):
    def setUp(self):
        self.logger = Mock()
        with patch('src.scraper.Scraper.get_team_info') as mock_team_info:
            self.scraper = Scraper(self.logger)
        self.scraper.non_sidebar_subs = []


    @patch('requests.get')
    def test_normal(self, mock_get):
        html = DummySidebarHTML()
        expected_subreddits = ["test1", "test2"]
        mock_get.return_value = html
        found_subs = self.scraper.get_team_info()
        self.assertEqual(expected_subreddits, found_subs)
        self.logger.error.assert_not_called()

    @patch('requests.get')
    def test_worked_second_time(self, mock_get):
        """ Check it works if it fails the first time"""
        html = DummySidebarHTML()
        empty_html = DummySidebarHTML()
        empty_html.clear()
        expected_subreddits = ["test1", "test2"]
        mock_get.side_effect = [empty_html, html]
        found_subs = self.scraper.get_team_info()
        self.assertEqual(expected_subreddits, found_subs)
        self.logger.error.assert_not_called()


    @patch('requests.get')
    @patch('builtins.open')
    def test_no_infinite_loop(self, mock_open, mock_get):
        """ Check it wont go into an infinite loop and will just break out if it can't access it"""
        empty_html = DummySidebarHTML()
        empty_html.clear()
        mock_get.return_value = empty_html
        found_subs = self.scraper.get_team_info()
        self.assertFalse(found_subs)
        self.assertEqual(len(found_subs), 0)
        self.logger.error.assert_called_with("Tried 50 times to get the sidebar tables and couldn't. "
                                             "Reverting to hardcoded subreddits.")

    @patch('requests.get')
    def test_reads_falls_back_on_file(self, mock_get):
        empty_html = DummySidebarHTML()
        empty_html.clear()
        mock_get.return_value = empty_html

        with patch('builtins.open', mock_open()) as m_open:
            expected_subreddits = ["test1", "test2"]
            m_open().readlines.return_value = ["test1\n", "test2\n"]
            found_subs = self.scraper.get_team_info()

            self.assertEqual(expected_subreddits, found_subs)
            self.logger.error.assert_called_with("Tried 50 times to get the sidebar tables and couldn't. "
                                                 "Reverting to hardcoded subreddits.")


    @patch('requests.get')
    def test_adds_non_sidebar(self, mock_get):
        self.scraper.non_sidebar_subs = ["test3", "test4"]
        html = DummySidebarHTML()
        expected_subreddits = ["test1", "test2", "test3", "test4"]
        mock_get.return_value = html
        found_subs = self.scraper.get_team_info()
        self.assertEqual(expected_subreddits, found_subs)
        self.logger.error.assert_not_called()

    @patch('requests.get')
    def test_table_bad_header(self, mock_get):
        """ Header row is unrecognisable, just read out of the provided file"""
        html = DummySidebarHTML()
        new_header = "badheader"
        html.replace_header(new_header)
        mock_get.return_value = html

        with patch('builtins.open', mock_open()) as m_open:
            expected_subreddits = ["test3", "test4"]
            m_open().readlines.return_value = ["test3\n", "test4\n"]
            found_subs = self.scraper.get_team_info()
            self.logger.error.assert_called_with(f"Header row has changed? Got in {new_header}.")

        self.assertEqual(expected_subreddits, found_subs)

    @patch('requests.get')
    def test_not_links_in_table(self, mock_get):
        """ No links in the table, will fall back on hardcoded subs """
        html = DummySidebarHTML(subs=[])
        mock_get.return_value = html

        with patch('builtins.open', mock_open()) as m_open:
            expected_subreddits = ["test3", "test4"]
            m_open().readlines.return_value = ["test3\n", "test4\n"]
            found_subs = self.scraper.get_team_info()

        self.assertEqual(expected_subreddits, found_subs)
        self.logger.error.assert_not_called()


    @patch('requests.get')
    def test_timeout(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = requests.exceptions.Timeout(error_message)

        found_id = self.scraper.get_team_info()

        self.logger.warning.assert_called_with(f"Requests Timeout in get_team_info. "
                                               f"<class 'requests.exceptions.Timeout'> - ('{error_message}',) "
                                               f"- {error_message}.")
        self.assertEqual(ExceptionCodes.RETRY, found_id)

    @patch('requests.get')
    def test_connection_error(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = requests.exceptions.ConnectionError(error_message)

        found_id = self.scraper.get_team_info()

        self.logger.warning.assert_called_with(f"Requests Connection Error in get_team_info. "
                                               f"<class 'requests.exceptions.ConnectionError'> - ('{error_message}',) "
                                               f"- {error_message}.")
        self.assertEqual(ExceptionCodes.RETRY, found_id)

    @patch('requests.get')
    def test_requests_error(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = requests.exceptions.RequestException(error_message)
        found_id = self.scraper.get_team_info()
        self.logger.warning.assert_called_with(f"Requests Unknown Error in get_team_info. "
                                               f"<class 'requests.exceptions.RequestException'> - ('{error_message}',) "
                                               f"- {error_message}.")
        self.assertEqual(ExceptionCodes.RETRY, found_id)

    @patch('requests.get')
    def test_chunked_error(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = requests.exceptions.ChunkedEncodingError(error_message)
        found_id = self.scraper.get_team_info()
        self.logger.warning.assert_called_with(f"Requests Chunked Encoding Error in get_team_info. "
                                               f"<class 'requests.exceptions.ChunkedEncodingError'> - ('{error_message}',) "
                                               f"- {error_message}.")
        self.assertEqual(ExceptionCodes.RETRY, found_id)

    @patch('requests.get')
    def test_error(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = Exception(error_message)
        found_id = self.scraper.get_team_info()
        self.logger.error.assert_called_with(f"Unknown Error in get_team_info. "
                                             f"<class 'Exception'> - ('{error_message}',) "
                                             f"- {error_message}.")
        self.assertEqual(ExceptionCodes.UNKNOWN_FAILURE, found_id)
