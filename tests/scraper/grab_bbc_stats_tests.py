from unittest import TestCase
from unittest.mock import patch, mock_open, Mock, call
import sys
import pathlib
import os
import requests

from src.scraper import Scraper
from src.codes import *
from tests.DummyClasses import *



class GrabStats(TestCase):
    def setUp(self):
        self.logger = Mock()
        with patch('src.scraper.Scraper.get_team_info') as mock_team_info:
            self.scraper = Scraper(self.logger)

        self.t1 = "home"
        self.t2 = "away"
        self.bbc_id = "000000"
        self.bbc_address = f"https://www.bbc.co.uk/sport/live/football/{self.bbc_id}"
        self.scraper.findBBCSiteSingle = Mock()
        self.scraper.findBBCSiteSingle.return_value = ExceptionCodes.SUCCESS, self.bbc_id, self.t1, self.t2



    @patch('requests.get')
    def test_normal(self, mock_get):
        """ Standard """
        html = DummyBBCStatsPage(("1", "2"), ("3", "4"), ("5", "6"), ("7", "8"), ("9", "10"))

        expected_result = "\n\n---------\n\n"
        expected_result += f'**MATCH STATS** | *via [BBC]({self.bbc_address})*\n\n'
        expected_result += f"||{self.t1}|{self.t2}|\n|:--|:--:|:--:|\n"
        expected_result += f"|Possession|1%|2%|\n"
        expected_result += f"|Shots|3|4|\n"
        expected_result += f"|Shots on Target|5|6|\n"
        expected_result += f"|Corners|7|8|\n"
        expected_result += f"|Fouls|9|10|\n"

        mock_get.return_value = html
        code, result = self.scraper.grabStats(self.t1, self.t2)
        self.assertEqual(ExceptionCodes.SUCCESS, code)
        self.assertEqual(expected_result, result)

    @patch('requests.get')
    def test_no_bbc_site(self, mock_get):
        """ No BBC Match Found, will return an empty string """
        html = DummyBBCStatsPage(("1", "2"), ("3", "4"), ("5", "6"), ("7", "8"), ("9", "10"))
        self.scraper.findBBCSiteSingle.return_value = ExceptionCodes.NO_MATCH, "", self.t1, self.t2

        mock_get.return_value = html
        code, result = self.scraper.grabStats(self.t1, self.t2)
        self.assertEqual(ExceptionCodes.FAILURE, code)
        self.assertEqual("", result)
        self.logger.info.assert_called_with(f"No BBC Site found for {self.t1} vs {self.t2}.")

    @patch('requests.get')
    def test_check_retries(self, mock_get):
        """ Finding the BBC Site had a weird issue and returned a retry code, check it retries"""
        html = DummyBBCStatsPage(("1", "2"), ("3", "4"), ("5", "6"), ("7", "8"), ("9", "10"))
        max = self.scraper.max_retries
        self.scraper.findBBCSiteSingle.side_effect = [(ExceptionCodes.RETRY, "", self.t1, self.t2),
                                                      (ExceptionCodes.SUCCESS, "000000", self.t1, self.t2)]

        expected_result = "\n\n---------\n\n"
        expected_result += f'**MATCH STATS** | *via [BBC]({self.bbc_address})*\n\n'
        expected_result += f"||{self.t1}|{self.t2}|\n|:--|:--:|:--:|\n"
        expected_result += f"|Possession|1%|2%|\n"
        expected_result += f"|Shots|3|4|\n"
        expected_result += f"|Shots on Target|5|6|\n"
        expected_result += f"|Corners|7|8|\n"
        expected_result += f"|Fouls|9|10|\n"

        mock_get.return_value = html
        code, result = self.scraper.grabStats(self.t1, self.t2)
        self.assertEqual(self.scraper.findBBCSiteSingle.call_count, 2)
        self.assertEqual(ExceptionCodes.SUCCESS, code)
        self.assertEqual(expected_result, result)
        calls = [call(f"Trying to get the BBC site. "f"On attempt 1 out of {self.scraper.max_retries}"),
                 call(f"Trying to get the BBC site. "f"On attempt 2 out of {self.scraper.max_retries}")]
        self.logger.info.assert_has_calls(calls)

    @patch('requests.get')
    def test_check_max_retries(self, mock_get):
        """ Check it keeps retrying """
        html = DummyBBCStatsPage(("1", "2"), ("3", "4"), ("5", "6"), ("7", "8"), ("9", "10"))

        self.scraper.max_retries = 5
        self.scraper.findBBCSiteSingle.side_effect = [(ExceptionCodes.RETRY, "", self.t1, self.t2),
                                                      (ExceptionCodes.RETRY, "", self.t1, self.t2),
                                                      (ExceptionCodes.RETRY, "", self.t1, self.t2),
                                                      (ExceptionCodes.RETRY, "", self.t1, self.t2),
                                                      (ExceptionCodes.SUCCESS, "000000", self.t1, self.t2)]

        expected_result = "\n\n---------\n\n"
        expected_result += f'**MATCH STATS** | *via [BBC]({self.bbc_address})*\n\n'
        expected_result += f"||{self.t1}|{self.t2}|\n|:--|:--:|:--:|\n"
        expected_result += f"|Possession|1%|2%|\n"
        expected_result += f"|Shots|3|4|\n"
        expected_result += f"|Shots on Target|5|6|\n"
        expected_result += f"|Corners|7|8|\n"
        expected_result += f"|Fouls|9|10|\n"

        mock_get.return_value = html
        code, result = self.scraper.grabStats(self.t1, self.t2)
        self.assertEqual(ExceptionCodes.SUCCESS, code)
        self.assertEqual(expected_result, result)
        calls = [call(f"Trying to get the BBC site. "f"On attempt 1 out of {self.scraper.max_retries}"),
                 call(f"Trying to get the BBC site. "f"On attempt 2 out of {self.scraper.max_retries}"),
                 call(f"Trying to get the BBC site. "f"On attempt 3 out of {self.scraper.max_retries}"),
                 call(f"Trying to get the BBC site. "f"On attempt 4 out of {self.scraper.max_retries}"),
                 call(f"Trying to get the BBC site. "f"On attempt 5 out of {self.scraper.max_retries}")]
        self.logger.info.assert_has_calls(calls)
        self.assertEqual(self.scraper.findBBCSiteSingle.call_count, 5)

    @patch('requests.get')
    def test_check_altered_max_retried(self, mock_get):
        """ Check it pays attention to the global variable """
        html = DummyBBCStatsPage(("1", "2"), ("3", "4"), ("5", "6"), ("7", "8"), ("9", "10"))

        self.scraper.max_retries = 7
        self.scraper.findBBCSiteSingle.side_effect = [(ExceptionCodes.RETRY, "", self.t1, self.t2),
                                                      (ExceptionCodes.RETRY, "", self.t1, self.t2),
                                                      (ExceptionCodes.RETRY, "", self.t1, self.t2),
                                                      (ExceptionCodes.RETRY, "", self.t1, self.t2),
                                                      (ExceptionCodes.RETRY, "", self.t1, self.t2),
                                                      (ExceptionCodes.RETRY, "", self.t1, self.t2),
                                                      (ExceptionCodes.SUCCESS, "000000", self.t1, self.t2)]

        expected_result = "\n\n---------\n\n"
        expected_result += f'**MATCH STATS** | *via [BBC]({self.bbc_address})*\n\n'
        expected_result += f"||{self.t1}|{self.t2}|\n|:--|:--:|:--:|\n"
        expected_result += f"|Possession|1%|2%|\n"
        expected_result += f"|Shots|3|4|\n"
        expected_result += f"|Shots on Target|5|6|\n"
        expected_result += f"|Corners|7|8|\n"
        expected_result += f"|Fouls|9|10|\n"

        mock_get.return_value = html
        code, result = self.scraper.grabStats(self.t1, self.t2)
        self.assertEqual(ExceptionCodes.SUCCESS, code)
        self.assertEqual(expected_result, result)
        calls = [call(f"Trying to get the BBC site. "f"On attempt 1 out of {self.scraper.max_retries}"),
                 call(f"Trying to get the BBC site. "f"On attempt 2 out of {self.scraper.max_retries}"),
                 call(f"Trying to get the BBC site. "f"On attempt 3 out of {self.scraper.max_retries}"),
                 call(f"Trying to get the BBC site. "f"On attempt 4 out of {self.scraper.max_retries}"),
                 call(f"Trying to get the BBC site. "f"On attempt 5 out of {self.scraper.max_retries}"),
                 call(f"Trying to get the BBC site. "f"On attempt 6 out of {self.scraper.max_retries}"),
                 call(f"Trying to get the BBC site. "f"On attempt 7 out of {self.scraper.max_retries}")]
        self.logger.info.assert_has_calls(calls)
        self.assertEqual(self.scraper.findBBCSiteSingle.call_count, 7)

    @patch('requests.get')
    def test_check_exceeds_reties(self, mock_get):
        """ Check it will stop after reaching the retry limit """
        html = DummyBBCStatsPage(("1", "2"), ("3", "4"), ("5", "6"), ("7", "8"), ("9", "10"))

        self.scraper.findBBCSiteSingle.side_effect = [(ExceptionCodes.RETRY, "", self.t1, self.t2),
                                                      (ExceptionCodes.RETRY, "", self.t1, self.t2),
                                                      (ExceptionCodes.RETRY, "", self.t1, self.t2),
                                                      (ExceptionCodes.RETRY, "", self.t1, self.t2),
                                                      (ExceptionCodes.RETRY, "", self.t1, self.t2),
                                                      (ExceptionCodes.SUCCESS, "000000", self.t1, self.t2)]

        expected_result = ""

        mock_get.return_value = html
        code, result = self.scraper.grabStats(self.t1, self.t2)
        self.assertEqual(ExceptionCodes.FAILURE, code)
        self.assertEqual(expected_result, result)
        calls = [call(f"Trying to get the BBC site. "f"On attempt 1 out of {self.scraper.max_retries}"),
                 call(f"Trying to get the BBC site. "f"On attempt 2 out of {self.scraper.max_retries}"),
                 call(f"Trying to get the BBC site. "f"On attempt 3 out of {self.scraper.max_retries}"),
                 call(f"Trying to get the BBC site. "f"On attempt 4 out of {self.scraper.max_retries}"),
                 call(f"Trying to get the BBC site. "f"On attempt 5 out of {self.scraper.max_retries}")]
        self.logger.info.assert_has_calls(calls)
        self.assertEqual(self.scraper.findBBCSiteSingle.call_count, 5)

    @patch('requests.get')
    def test_non_live_game(self, mock_get):
        """ Test it attempts the non live page """
        html = DummyBBCStatsPage(("1", "2"), ("3", "4"), ("5", "6"), ("7", "8"), ("9", "10"), status_code=404)

        expected_result = ""

        mock_get.return_value = html
        code, result = self.scraper.grabStats(self.t1, self.t2)
        self.assertEqual(ExceptionCodes.FAILURE, code)
        self.assertEqual(expected_result, result)
        url1 = f"https://www.bbc.co.uk/sport/live/football/{self.bbc_id}"
        url2 = f"https://www.bbc.co.uk/sport/football/{self.bbc_id}"
        calls = [call(url1, timeout=15, stream=True),
                 call(url2, timeout=15, stream=True)]
        mock_get.assert_has_calls(calls)

    @patch('requests.get')
    def test_missing_possession(self, mock_get):
        """ Wont include a row about possession """
        html = DummyBBCStatsPage(("1", "2"), ("3", "4"), ("5", "6"), ("7", "8"), ("9", "10"))
        html.remove_row("Possession")
        expected_result = "\n\n---------\n\n"
        expected_result += f'**MATCH STATS** | *via [BBC]({self.bbc_address})*\n\n'
        expected_result += f"||{self.t1}|{self.t2}|\n|:--|:--:|:--:|\n"
        # expected_result += f"|Possession|1%|2%|\n"
        expected_result += f"|Shots|3|4|\n"
        expected_result += f"|Shots on Target|5|6|\n"
        expected_result += f"|Corners|7|8|\n"
        expected_result += f"|Fouls|9|10|\n"


        mock_get.return_value = html
        code, result = self.scraper.grabStats(self.t1, self.t2)
        self.assertEqual(ExceptionCodes.SUCCESS, code)
        self.assertEqual(expected_result, result)

    @patch('requests.get')
    def test_missing_shots(self, mock_get):
        """ Wont include a row about shots """
        html = DummyBBCStatsPage(("1", "2"), ("3", "4"), ("5", "6"), ("7", "8"), ("9", "10"))
        html.remove_row("shots")

        expected_result = "\n\n---------\n\n"
        expected_result += f'**MATCH STATS** | *via [BBC]({self.bbc_address})*\n\n'
        expected_result += f"||{self.t1}|{self.t2}|\n|:--|:--:|:--:|\n"
        expected_result += f"|Possession|1%|2%|\n"
        # expected_result += f"|Shots|3|4|\n"
        expected_result += f"|Shots on Target|5|6|\n"
        expected_result += f"|Corners|7|8|\n"
        expected_result += f"|Fouls|9|10|\n"

        mock_get.return_value = html

        code, result = self.scraper.grabStats(self.t1, self.t2)
        self.assertEqual(ExceptionCodes.SUCCESS, code)
        self.assertEqual(expected_result, result)

    @patch('requests.get')
    def test_missing_shots_on_target(self, mock_get):
        """ Wont include a row about shots on target """
        html = DummyBBCStatsPage(("1", "2"), ("3", "4"), ("5", "6"), ("7", "8"), ("9", "10"))
        html.remove_row("shots on target")

        expected_result = "\n\n---------\n\n"
        expected_result += f'**MATCH STATS** | *via [BBC]({self.bbc_address})*\n\n'
        expected_result += f"||{self.t1}|{self.t2}|\n|:--|:--:|:--:|\n"
        expected_result += f"|Possession|1%|2%|\n"
        expected_result += f"|Shots|3|4|\n"
        # expected_result += f"|Shots on Target|5|6|\n"
        expected_result += f"|Corners|7|8|\n"
        expected_result += f"|Fouls|9|10|\n"

        mock_get.return_value = html

        code, result = self.scraper.grabStats(self.t1, self.t2)
        self.assertEqual(ExceptionCodes.SUCCESS, code)
        self.assertEqual(expected_result, result)

    @patch('requests.get')
    def test_missing_corners(self, mock_get):
        """ Wont include a row about corners """
        html = DummyBBCStatsPage(("1", "2"), ("3", "4"), ("5", "6"), ("7", "8"), ("9", "10"))
        html.remove_row("corners")

        expected_result = "\n\n---------\n\n"
        expected_result += f'**MATCH STATS** | *via [BBC]({self.bbc_address})*\n\n'
        expected_result += f"||{self.t1}|{self.t2}|\n|:--|:--:|:--:|\n"
        expected_result += f"|Possession|1%|2%|\n"
        expected_result += f"|Shots|3|4|\n"
        expected_result += f"|Shots on Target|5|6|\n"
        # expected_result += f"|Corners|7|8|\n"
        expected_result += f"|Fouls|9|10|\n"

        mock_get.return_value = html

        code, result = self.scraper.grabStats(self.t1, self.t2)
        self.assertEqual(ExceptionCodes.SUCCESS, code)
        self.assertEqual(expected_result, result)

    @patch('requests.get')
    def test_missing_fouls(self, mock_get):
        """ Wont include a row about fouls """
        html = DummyBBCStatsPage(("1", "2"), ("3", "4"), ("5", "6"), ("7", "8"), ("9", "10"))
        html.remove_row("fouls")

        expected_result = "\n\n---------\n\n"
        expected_result += f'**MATCH STATS** | *via [BBC]({self.bbc_address})*\n\n'
        expected_result += f"||{self.t1}|{self.t2}|\n|:--|:--:|:--:|\n"
        expected_result += f"|Possession|1%|2%|\n"
        expected_result += f"|Shots|3|4|\n"
        expected_result += f"|Shots on Target|5|6|\n"
        expected_result += f"|Corners|7|8|\n"
        # expected_result += f"|Fouls|9|10|\n"

        mock_get.return_value = html

        code, result = self.scraper.grabStats(self.t1, self.t2)
        self.assertEqual(ExceptionCodes.SUCCESS, code)
        self.assertEqual(expected_result, result)

    @patch('requests.get')
    def test_non_number(self, mock_get):
        """ Should exclude rows with the number but keep the rest """
        html = DummyBBCStatsPage(("a", "2"), ("b", "4"), ("5", "6"), ("7", "8"), ("9", "10"))

        expected_result = "\n\n---------\n\n"
        expected_result += f'**MATCH STATS** | *via [BBC]({self.bbc_address})*\n\n'
        expected_result += f"||{self.t1}|{self.t2}|\n|:--|:--:|:--:|\n"
        # expected_result += f"|Possession|1%|2%|\n"
        # expected_result += f"|Shots|3|4|\n"
        expected_result += f"|Shots on Target|5|6|\n"
        expected_result += f"|Corners|7|8|\n"
        expected_result += f"|Fouls|9|10|\n"

        mock_get.return_value = html

        code, result = self.scraper.grabStats(self.t1, self.t2)
        self.assertEqual(ExceptionCodes.SUCCESS, code)
        self.assertEqual(expected_result, result)
        calls = [call(f"Could not find two numbers in a row. Row looks like this \"Possession a 2\""),
                 call(f"Could not find two numbers in a row. Row looks like this \"Shots b 4\"")]

        self.logger.warning.assert_has_calls(calls)

    @patch('requests.get')
    def test_all_rows_missing(self, mock_get):
        """ Should return an empty string """
        html = DummyBBCStatsPage(("1", "2"), ("3", "4"), ("5", "6"), ("7", "8"), ("9", "10"))
        html.remove_row("Possession")
        html.remove_row("Shots")
        html.remove_row("Shots on Target")
        html.remove_row("Corners")
        html.remove_row("Fouls")
        expected_result = ""

        mock_get.return_value = html

        code, result = self.scraper.grabStats(self.t1, self.t2)
        self.assertEqual(expected_result, result)
        self.assertEqual(ExceptionCodes.FAILURE, code)

        self.logger.warning.assert_called_with(f"Managed to find a stats table for {self.t1} vs {self.t2} "
                                               f"but there was no data.")

    # @patch('requests.get')
    # def test_timeout(self, mock_get):
    #     error_message = "oh no deary me"
    #     mock_get.side_effect = requests.exceptions.Timeout(error_message)
    #     found_id = self.scraper.grabStats(self.t1, self.t2)
    #     self.logger.warning.assert_called_with(f"Requests Timeout in grabStats. "
    #                                            f"<class 'requests.exceptions.Timeout'> - ('{error_message}',) "
    #                                            f"- {error_message}.")
    #     self.assertEqual(ExceptionCodes.RETRY, found_id)

    # @patch('requests.get')
    # def test_connection_error(self, mock_get):
    #     error_message = "oh no deary me"
    #     mock_get.side_effect = requests.exceptions.ConnectionError(error_message)
    #     found_id = self.scraper.grabStats(self.t1, self.t2)
    #     self.logger.warning.assert_called_with(f"Requests Connection Error in grabStats. "
    #                                            f"<class 'requests.exceptions.ConnectionError'> - ('{error_message}',) "
    #                                            f"- {error_message}.")
    #     self.assertEqual(ExceptionCodes.RETRY, found_id)

    # @patch('requests.get')
    # def test_requests_error(self, mock_get):
    #     error_message = "oh no deary me"
    #     mock_get.side_effect = requests.exceptions.RequestException(error_message)
    #     found_id = self.scraper.grabStats(self.t1, self.t2)
    #     self.logger.warning.assert_called_with(f"Requests Unknown Error in grabStats. "
    #                                            f"<class 'requests.exceptions.RequestException'> - ('{error_message}',) "
    #                                            f"- {error_message}.")
    #     self.assertEqual(ExceptionCodes.RETRY, found_id)

    # @patch('requests.get')
    # def test_chunked_error(self, mock_get):
    #     error_message = "oh no deary me"
    #     mock_get.side_effect = requests.exceptions.ChunkedEncodingError(error_message)
    #     found_id = self.scraper.grabStats(self.t1, self.t2)
    #     self.logger.warning.assert_called_with(f"Requests Chunked Encoding Error in grabStats. "
    #                                            f"<class 'requests.exceptions.ChunkedEncodingError'> - ('{error_message}',) "
    #                                            f"- {error_message}.")
    #     self.assertEqual(ExceptionCodes.RETRY, found_id)

    # @patch('requests.get')
    # def test_error(self, mock_get):
    #     error_message = "oh no deary me"
    #     mock_get.side_effect = Exception(error_message)
    #     found_id = self.scraper.grabStats(self.t1, self.t2)
    #     self.logger.error.assert_called_with(f"Unknown Error in grabStats. "
    #                                          f"<class 'Exception'> - ('{error_message}',) "
    #                                          f"- {error_message}.")
    #     self.assertEqual(ExceptionCodes.UNKNOWN_FAILURE, found_id)