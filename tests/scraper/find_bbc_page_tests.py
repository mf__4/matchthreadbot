from unittest import TestCase
from unittest.mock import patch, mock_open, Mock
import sys
import pathlib
import os
import requests

from src.scraper import Scraper
from src.codes import *
from tests.DummyClasses import *



class FindBBCPage(TestCase):
    def setUp(self):
        self.logger = Mock()
        with patch('src.scraper.Scraper.get_team_info') as mock_team_info:
            self.scraper = Scraper(self.logger)

        self.t1 = "home"
        self.t2 = "away"
        self.standard_id = "123"

    @patch('requests.get')
    def test_normal(self, mock_get):
        games = {self.standard_id: (self.t1, self.t2)}
        html = DummyBBCScorePage(games)
        mock_get.return_value = html
        code, game_id, home, away = self.scraper.findBBCSiteSingle(self.t1, self.t2)
        self.assertEqual(ExceptionCodes.SUCCESS, code)
        self.assertEqual(self.standard_id, game_id)
        self.assertEqual(self.t1, home)
        self.assertEqual(self.t2, away)

    @patch('requests.get')
    def test_backwards(self, mock_get):
        games = {self.standard_id: (self.t1, self.t2)}
        html = DummyBBCScorePage(games)
        mock_get.return_value = html
        code, game_id, home, away = self.scraper.findBBCSiteSingle(self.t2, self.t1)
        self.assertEqual(ExceptionCodes.SUCCESS, code)
        self.assertEqual(self.standard_id, game_id)
        self.assertEqual(self.t1, home)
        self.assertEqual(self.t2, away)

    @patch('requests.get')
    def test_doesnt_exist(self, mock_get):
        games = {}
        html = DummyBBCScorePage(games)
        mock_get.return_value = html
        code, game_id, home, away = self.scraper.findBBCSiteSingle(self.t1, self.t2)
        self.assertEqual(ExceptionCodes.NO_MATCH, code)
        self.assertEqual("", game_id)
        self.assertEqual("", home)
        self.assertEqual("", away)

    @patch('requests.get')
    def test_mult_games(self, mock_get):
        games = {self.standard_id: (self.t1, self.t2), "0": ("team", "other")}
        html = DummyBBCScorePage(games)
        mock_get.return_value = html
        code, game_id, home, away = self.scraper.findBBCSiteSingle(self.t1, self.t2)
        self.assertEqual(ExceptionCodes.SUCCESS, code)
        self.assertEqual(self.standard_id, game_id)
        self.assertEqual(self.t1, home)
        self.assertEqual(self.t2, away)

    @patch('requests.get')
    def test_mult_games_valid_second(self, mock_get):
        games = { "0": ("team", "other"), self.standard_id: (self.t1, self.t2)}
        html = DummyBBCScorePage(games)
        mock_get.return_value = html
        code, game_id, home, away = self.scraper.findBBCSiteSingle(self.t1, self.t2)
        self.assertEqual(ExceptionCodes.SUCCESS, code)
        self.assertEqual(self.standard_id, game_id)
        self.assertEqual(self.t1, home)
        self.assertEqual(self.t2, away)

    @patch('requests.get')
    def test_mult_games_pick_closest(self, mock_get):
        games = {"0": (f"{self.t1} FC", self.t2), self.standard_id: (self.t1, self.t2)}
        html = DummyBBCScorePage(games)
        mock_get.return_value = html
        code, game_id, home, away = self.scraper.findBBCSiteSingle(self.t1, self.t2)
        self.assertEqual(ExceptionCodes.SUCCESS, code)
        self.assertEqual(self.standard_id, game_id)
        self.assertEqual(self.t1, home)
        self.assertEqual(self.t2, away)

    @patch('requests.get')
    def test_multiple_links(self, mock_get):
        games = {self.standard_id: (self.t1, self.t2)}
        html = DummyBBCScorePage(games)
        html.add_link(self.standard_id, "example.com")
        mock_get.return_value = html
        code, game_id, home, away = self.scraper.findBBCSiteSingle(self.t1, self.t2)
        self.logger.error.assert_called_with(f"MORE THAN ONE LINK ON BBC PAGE FOR {self.t1} vs {self.t2}")
        self.assertEqual(ExceptionCodes.NO_MATCH, code)
        self.assertEqual("", game_id)
        self.assertEqual("", home)
        self.assertEqual("", away)

    @patch('requests.get')
    def test_no_links(self, mock_get):
        games = {self.standard_id: (self.t1, self.t2)}
        html = DummyBBCScorePage(games)
        html.remove_link(self.standard_id)
        mock_get.return_value = html
        code, game_id, home, away = self.scraper.findBBCSiteSingle(self.t1, self.t2)
        self.logger.debug.assert_called_with(f"NO LINK ON BBC PAGE FOR {self.t1} vs {self.t2}")
        self.assertEqual(ExceptionCodes.NO_MATCH, code)
        self.assertEqual("", game_id)
        self.assertEqual("", home)
        self.assertEqual("", away)

    @patch('requests.get')
    def test_bad_match_id(self, mock_get):
        """ Assuming match id is just a number, check theres not a letter in the middle of it """
        games = {f"12a34": (self.t1, self.t2)}
        html = DummyBBCScorePage(games)
        mock_get.return_value = html
        code, game_id, home, away = self.scraper.findBBCSiteSingle(self.t1, self.t2)
        self.logger.error.assert_called_with(f"MORE THAN ONE MATCHID ON BBC PAGE FOR {self.t1} vs {self.t2}")
        self.assertEqual(ExceptionCodes.NO_MATCH, code)
        self.assertEqual("", game_id)
        self.assertEqual("", home)
        self.assertEqual("", away)

    @patch('requests.get')
    def test_only_one_team(self, mock_get):
        """ Only show one team """
        games = {self.standard_id: (self.t1, self.t2)}
        html = DummyBBCScorePage(games)
        html.remove_team(self.t1)
        mock_get.return_value = html
        code, game_id, home, away = self.scraper.findBBCSiteSingle(self.t1, self.t2)
        self.logger.error.assert_called_with(f"BBC page doesn't have exactly two teams, 1 team(s) found.")
        self.assertEqual(ExceptionCodes.NO_MATCH, code)
        self.assertEqual("", game_id)
        self.assertEqual("", home)
        self.assertEqual("", away)

    @patch('requests.get')
    def test_no_teams(self, mock_get):
        """ No teams """
        games = {self.standard_id: (self.t1, self.t2)}
        html = DummyBBCScorePage(games)
        html.remove_team(self.t1)
        html.remove_team(self.t2)
        mock_get.return_value = html
        code, game_id, home, away = self.scraper.findBBCSiteSingle(self.t1, self.t2)
        self.logger.error.assert_called_with(f"BBC page doesn't have exactly two teams, 0 team(s) found.")
        self.assertEqual(ExceptionCodes.NO_MATCH, code)
        self.assertEqual("", game_id)
        self.assertEqual("", home)
        self.assertEqual("", away)

    @patch('requests.get')
    def test_none_home_team(self, mock_get):
        """ None teams """
        games = {self.standard_id: (self.t1, self.t2)}
        html = DummyBBCScorePage(games)
        html.remove_team_span(self.t1)
        mock_get.return_value = html
        code, game_id, home, away = self.scraper.findBBCSiteSingle(self.t1, self.t2)
        self.logger.error.assert_called_with(f"BBC PAGE ERROR. HOME OR AWAY TEAM FOR {self.t1} vs {self.t2} IS NONE")
        self.assertEqual(ExceptionCodes.NO_MATCH, code)
        self.assertEqual("", game_id)
        self.assertEqual("", home)
        self.assertEqual("", away)

    @patch('requests.get')
    def test_none_away_team(self, mock_get):
        """ None teams """
        games = {self.standard_id: (self.t1, self.t2)}
        html = DummyBBCScorePage(games)
        html.remove_team_span(self.t2)
        mock_get.return_value = html
        code, game_id, home, away = self.scraper.findBBCSiteSingle(self.t1, self.t2)
        self.logger.error.assert_called_with(f"BBC PAGE ERROR. HOME OR AWAY TEAM FOR {self.t1} vs {self.t2} IS NONE")
        self.assertEqual(ExceptionCodes.NO_MATCH, code)
        self.assertEqual("", game_id)
        self.assertEqual("", home)
        self.assertEqual("", away)

    @patch('requests.get')
    def test_none_both_team(self, mock_get):
        """ None teams """
        games = {self.standard_id: (self.t1, self.t2)}
        html = DummyBBCScorePage(games)
        html.remove_team_span(self.t1)
        html.remove_team_span(self.t2)
        mock_get.return_value = html
        code, game_id, home, away = self.scraper.findBBCSiteSingle(self.t1, self.t2)
        self.logger.error.assert_called_with(f"BBC PAGE ERROR. HOME OR AWAY TEAM FOR {self.t1} vs {self.t2} IS NONE")
        self.assertEqual(ExceptionCodes.NO_MATCH, code)
        self.assertEqual("", game_id)
        self.assertEqual("", home)
        self.assertEqual("", away)

    @patch('requests.get')
    def test_postponed(self, mock_get):
        games = {self.standard_id: (self.t1, self.t2)}
        html = DummyBBCScorePage(games)
        html.add_game_status(self.standard_id, "Match postponed")
        mock_get.return_value = html
        code, game_id, home, away = self.scraper.findBBCSiteSingle(self.t1, self.t2)
        self.logger.debug.assert_called_with(f"{self.t1} vs {self.t2} is postponed.")
        self.assertEqual(ExceptionCodes.NO_MATCH, code)
        self.assertEqual("", game_id)
        self.assertEqual("", home)
        self.assertEqual("", away)



    @patch('requests.get')
    def test_timeout(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = requests.exceptions.Timeout(error_message)
        found_id = self.scraper.findBBCSiteSingle(self.t1, self.t2)
        self.logger.warning.assert_called_with(f"Requests Timeout in findBBCSiteSingle. "
                                               f"<class 'requests.exceptions.Timeout'> - ('{error_message}',) "
                                               f"- {error_message}.")
        self.assertEqual(ExceptionCodes.RETRY, found_id)

    @patch('requests.get')
    def test_connection_error(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = requests.exceptions.ConnectionError(error_message)
        found_id = self.scraper.findBBCSiteSingle(self.t1, self.t2)
        self.logger.warning.assert_called_with(f"Requests Connection Error in findBBCSiteSingle. "
                                               f"<class 'requests.exceptions.ConnectionError'> - ('{error_message}',) "
                                               f"- {error_message}.")
        self.assertEqual(ExceptionCodes.RETRY, found_id)

    @patch('requests.get')
    def test_requests_error(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = requests.exceptions.RequestException(error_message)
        found_id = self.scraper.findBBCSiteSingle(self.t1, self.t2)
        self.logger.warning.assert_called_with(f"Requests Unknown Error in findBBCSiteSingle. "
                                               f"<class 'requests.exceptions.RequestException'> - ('{error_message}',) "
                                               f"- {error_message}.")
        self.assertEqual(ExceptionCodes.RETRY, found_id)

    @patch('requests.get')
    def test_chunked_error(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = requests.exceptions.ChunkedEncodingError(error_message)
        found_id = self.scraper.findBBCSiteSingle(self.t1, self.t2)
        self.logger.warning.assert_called_with(f"Requests Chunked Encoding Error in findBBCSiteSingle. "
                                               f"<class 'requests.exceptions.ChunkedEncodingError'> - ('{error_message}',) "
                                               f"- {error_message}.")
        self.assertEqual(ExceptionCodes.RETRY, found_id)

    @patch('requests.get')
    def test_error(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = Exception(error_message)
        found_id = self.scraper.findBBCSiteSingle(self.t1, self.t2)
        self.logger.error.assert_called_with(f"Unknown Error in findBBCSiteSingle. "
                                             f"<class 'Exception'> - ('{error_message}',) "
                                             f"- {error_message}.")
        self.assertEqual(ExceptionCodes.UNKNOWN_FAILURE, found_id)

