from unittest import TestCase
from unittest.mock import patch, mock_open, Mock
import sys
import pathlib
import os
import requests

from src.scraper import Scraper
from src.codes import *
from tests.DummyClasses import *

class GuessRightMatchTests(TestCase):
    def setUp(self):
        self.logger = Mock()
        with patch('src.scraper.Scraper.get_team_info') as mock_team_info:
            self.scraper = Scraper(self.logger)
        self.scraper.getStatus = Mock()
        self.goal_teams = ("home", "away")


    def test_normal(self):
        """ Will return the only game """
        possibles = {"111111": ("home", "away")}
        val = self.scraper.guessRightMatch(possibles, self.goal_teams)
        self.assertEqual(val, "111111")

    def test_both_playing(self):
        """ Will guess 222222 as closer to goal teams"""
        possibles = {"111111": ("home", "away"),
                     "222222": ("home united", " FC away")}
        self.goal_teams = "home united", "FC away"
        val = self.scraper.guessRightMatch(possibles, self.goal_teams)
        self.assertEqual(val, "222222")

    def test_similar_teams(self):
        """ Will guess 222222 as closer to goal teams"""
        possibles = {"111111": ("home united", "away"),
                     "222222": ("home of united", " FC away")}
        self.goal_teams = "home united", "FC away"
        val = self.scraper.guessRightMatch(possibles, self.goal_teams)
        self.assertEqual(val, "222222")


    def test_similar_teams_2(self):
        """ Will guess 222222 as the non FC one is wanted"""
        possibles = {"111111": ("home united", "FC away"),
                     "222222": ("home united", "away")}
        self.goal_teams = "home united", "away"
        val = self.scraper.guessRightMatch(possibles, self.goal_teams)
        self.assertEqual(val, "222222")

    def test_similar_teams_3(self):
        """ Will guess 222222 as the away team is better"""
        possibles = {"111111": ("home united", "other"),
                     "222222": ("home united", "away athletic")}
        self.goal_teams = "home", "away"
        val = self.scraper.guessRightMatch(possibles, self.goal_teams)
        self.assertEqual(val, "222222")

    def test_similar_teams_4(self):
        """ Will guess 222222 as the away team is better, despite home matching exactly"""
        possibles = {"111111": ("home of united", "other"),
                     "222222": ("home of the united", "away athletic")}
        self.goal_teams = "home of united", "away"
        val = self.scraper.guessRightMatch(possibles, self.goal_teams)
        self.assertEqual(val, "222222")

    def test_similar_teams_5(self):
        """ Will guess 222222 as the away team is better"""
        possibles = {"111111": ("Home U21", "Other U21"),
                     "222222": ("Home", "Away"),
                     "333333": ("Home U17", "Some U17")}
        self.goal_teams = "Home", "Away"
        val = self.scraper.guessRightMatch(possibles, self.goal_teams)
        self.assertEqual(val, "222222")