from unittest import TestCase
from unittest.mock import patch, mock_open, Mock, call
import sys
import pathlib
import os
import requests

from src.scraper import Scraper
from src.codes import *
from tests.DummyClasses import *



class GetTimes(TestCase):
    def setUp(self):
        self.logger = Mock()
        with patch('src.scraper.Scraper.get_team_info') as mock_team_info:
            self.scraper = Scraper(self.logger)

    def test_normal(self):
        time = self.scraper.getTimes("15:00")
        expected_time = (15, 0)

        self.assertEqual(expected_time, time)

    def test_normal_2(self):
        time = self.scraper.getTimes("12:45")
        expected_time = (12, 45)

        self.assertEqual(expected_time, time)

    def test_midnight(self):
        time = self.scraper.getTimes("00:00")
        expected_time = (0, 0)

        self.assertEqual(expected_time, time)

    def test_midnight_2(self):
        time = self.scraper.getTimes("24:00")
        expected_time = (24, 0)

        self.assertEqual(expected_time, time)

    def test_only_hour(self):
        time = self.scraper.getTimes("12")
        expected_time = (12, 0)

        self.assertEqual(expected_time, time)

    def test_too_many_colons(self):
        time = self.scraper.getTimes("12:00:12")
        expected_time = (ExceptionCodes.FAILURE, ExceptionCodes.FAILURE)

        self.assertEqual(expected_time, time)

    def test_letters_minutes(self):
        time = self.scraper.getTimes("12:aa")
        expected_time = (12, ExceptionCodes.FAILURE)

        self.assertEqual(expected_time, time)

    def test_letters_hours(self):
        time = self.scraper.getTimes("aa:30")
        expected_time = (ExceptionCodes.FAILURE, 30)

        self.assertEqual(expected_time, time)

    def test_letters_only_hours(self):
        time = self.scraper.getTimes("aa")
        expected_time = (ExceptionCodes.FAILURE, ExceptionCodes.FAILURE)

        self.assertEqual(expected_time, time)
