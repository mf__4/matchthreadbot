from unittest import TestCase
from unittest.mock import patch, mock_open, Mock
import sys
import pathlib
import os
import requests

from src.scraper import Scraper
from src.codes import *
from tests.DummyClasses import *

class FindMatchSiteTests(TestCase):
    def setUp(self):
        self.logger = Mock()
        with patch('src.scraper.Scraper.get_team_info') as mock_team_info:
            self.scraper = Scraper(self.logger)
        self.team1 = "home"
        self.team2 = "away"

    @patch('requests.get')
    def test_normal(self, mock_get):
        gameID = "000000"
        html = DummyMatchesHTML(self.team1, self.team2, gameid=gameID)
        mock_get.return_value = html
        found_id = self.scraper.findMatchSite(self.team1, self.team2)
        self.assertEqual(gameID, found_id)

    @patch('requests.get')
    def test_wrong_way_round(self, mock_get):
        gameID = "000000"
        html = DummyMatchesHTML(self.team1, self.team2, gameid=gameID)
        mock_get.return_value = html
        found_id = self.scraper.findMatchSite(self.team2, self.team1)
        self.assertEqual(gameID, found_id)

    @patch('requests.get')
    def test_only_home(self, mock_get):
        gameID = "000000"
        html = DummyMatchesHTML(self.team1, self.team2, gameid=gameID)
        mock_get.return_value = html
        found_id = self.scraper.findMatchSite(self.team1, "")
        self.assertEqual(gameID, found_id)

    @patch('requests.get')
    def test_only_away(self, mock_get):
        gameID = "000000"
        html = DummyMatchesHTML(self.team1, self.team2, gameid=gameID)
        mock_get.return_value = html
        found_id = self.scraper.findMatchSite("", self.team2)
        self.assertEqual(gameID, found_id)

    @patch('requests.get')
    def test_only_home_wrong_way(self, mock_get):
        gameID = "000000"
        html = DummyMatchesHTML(self.team1, self.team2, gameid=gameID)
        mock_get.return_value = html
        found_id = self.scraper.findMatchSite("", self.team1)
        self.assertEqual(gameID, found_id)

    @patch('requests.get')
    def test_only_away_wrong_way(self, mock_get):
        gameID = "000000"
        html = DummyMatchesHTML(self.team1, self.team2, gameid=gameID)
        mock_get.return_value = html
        found_id = self.scraper.findMatchSite(self.team2, "")
        self.assertEqual(gameID, found_id)

    @patch('requests.get')
    def test_missing_suffix(self, mock_get):
        gameID = "000000"
        html = DummyMatchesHTML(self.team1 + " United", self.team2, gameid=gameID)
        mock_get.return_value = html
        found_id = self.scraper.findMatchSite(self.team1, self.team2)
        self.assertEqual(gameID, found_id)

    @patch('requests.get')
    def test_missing_prefix(self, mock_get):
        gameID = "000000"
        html = DummyMatchesHTML("FC " + self.team1, self.team2, gameid=gameID)
        mock_get.return_value = html
        found_id = self.scraper.findMatchSite(self.team1, self.team2)
        self.assertEqual(gameID, found_id)

    @patch('requests.get')
    def test_not_there(self, mock_get):
        html = DummyMatchesHTML(self.team1, self.team2)
        mock_get.return_value = html
        found_id = self.scraper.findMatchSite("team 1", "team 2")
        self.assertEqual(ExceptionCodes.NO_MATCH, found_id)

    @patch('requests.get')
    def test_none_both(self, mock_get):
        html = DummyMatchesHTML(self.team1, self.team2)
        mock_get.return_value = html
        found_id = self.scraper.findMatchSite(None, None)
        self.assertEqual(ExceptionCodes.NO_MATCH, found_id)

    @patch('requests.get')
    def test_none_home(self, mock_get):
        gameID = "000000"
        html = DummyMatchesHTML(self.team1, self.team2, gameid=gameID)
        mock_get.return_value = html
        found_id = self.scraper.findMatchSite(None, self.team2)
        self.assertEqual(gameID, found_id)

    @patch('requests.get')
    def test_none_away(self, mock_get):
        gameID = "000000"
        html = DummyMatchesHTML(self.team1, self.team2, gameid=gameID)
        mock_get.return_value = html
        found_id = self.scraper.findMatchSite(self.team1, None)
        self.assertEqual(gameID, found_id)

    @patch('requests.get')
    def test_none_home_wrong_way(self, mock_get):
        gameID = "000000"
        html = DummyMatchesHTML(self.team1, self.team2, gameid=gameID)
        mock_get.return_value = html
        found_id = self.scraper.findMatchSite(None, self.team1)
        self.assertEqual(gameID, found_id)

    @patch('requests.get')
    def test_none_away_wrong_way(self, mock_get):
        gameID = "000000"
        html = DummyMatchesHTML(self.team1, self.team2, gameid=gameID)
        mock_get.return_value = html
        found_id = self.scraper.findMatchSite(self.team2, None)
        self.assertEqual(gameID, found_id)

    @patch('requests.get')
    def test_selects_best_first(self, mock_get):
        gameID = "000000"
        html = DummyMatchesHTML(self.team1, self.team2, gameid=gameID)
        html.add_game("bad", "bad", "123456")
        mock_get.return_value = html
        found_id = self.scraper.findMatchSite(self.team1, self.team2)
        self.assertEqual(gameID, found_id)

    @patch('requests.get')
    def test_selects_best_second(self, mock_get):
        gameID = "000000"
        html = DummyMatchesHTML("bad", "bad", "123456")
        html.add_game(self.team1, self.team2, gameid=gameID)
        mock_get.return_value = html
        found_id = self.scraper.findMatchSite(self.team1, self.team2)
        self.assertEqual(gameID, found_id)

    @patch('requests.get')
    def test_selects_best_home_closer(self, mock_get):
        gameID = "000000"
        html = DummyMatchesHTML(self.team1, self.team2, gameid=gameID)
        html.add_game(self.team1 + " Utd", self.team2, gameid="000000")
        mock_get.return_value = html
        found_id = self.scraper.findMatchSite(self.team1, self.team2)
        self.assertEqual(gameID, found_id)

    @patch('requests.get')
    def test_selects_best_away_closer(self, mock_get):
        gameID = "000000"
        html = DummyMatchesHTML(self.team1, self.team2, gameid=gameID)
        html.add_game(self.team1, self.team2 + " Utd", gameid="000000")
        mock_get.return_value = html
        found_id = self.scraper.findMatchSite(self.team1, self.team2)
        self.assertEqual(gameID, found_id)


    @patch('requests.get')
    def test_timeout(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = requests.exceptions.Timeout(error_message)
        found_id = self.scraper.findMatchSite(self.team1, None)

        self.logger.warning.assert_called_with(f"Requests Timeout in findMatchSite. "
                                               f"<class 'requests.exceptions.Timeout'> - ('{error_message}',) "
                                               f"- {error_message}.")
        self.assertEqual(ExceptionCodes.RETRY, found_id)

    @patch('requests.get')
    def test_connection_error(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = requests.exceptions.ConnectionError(error_message)

        found_id = self.scraper.findMatchSite(self.team1, None)

        self.logger.warning.assert_called_with(f"Requests Connection Error in findMatchSite. "
                                               f"<class 'requests.exceptions.ConnectionError'> - ('{error_message}',) "
                                               f"- {error_message}.")
        self.assertEqual(ExceptionCodes.RETRY, found_id)

    @patch('requests.get')
    def test_requests_error(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = requests.exceptions.RequestException(error_message)
        found_id = self.scraper.findMatchSite(self.team1, None)
        self.logger.warning.assert_called_with(f"Requests Unknown Error in findMatchSite. "
                                               f"<class 'requests.exceptions.RequestException'> - ('{error_message}',) "
                                               f"- {error_message}.")
        self.assertEqual(ExceptionCodes.RETRY, found_id)

    @patch('requests.get')
    def test_chunked_error(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = requests.exceptions.ChunkedEncodingError(error_message)
        found_id = self.scraper.findMatchSite(self.team1, self.team2)
        self.logger.warning.assert_called_with(f"Requests Chunked Encoding Error in findMatchSite. "
                                               f"<class 'requests.exceptions.ChunkedEncodingError'> - ('{error_message}',) "
                                               f"- {error_message}.")
        self.assertEqual(ExceptionCodes.RETRY, found_id)

    @patch('requests.get')
    def test_error(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = Exception(error_message)
        found_id = self.scraper.findMatchSite(self.team1, None)
        self.logger.error.assert_called_with(f"Unknown Error in findMatchSite. "
                                             f"<class 'Exception'> - ('{error_message}',) "
                                             f"- {error_message}.")
        self.assertEqual(ExceptionCodes.UNKNOWN_FAILURE, found_id)


class FindMatchSiteSingleTests(TestCase):
    def setUp(self):
        self.logger = Mock()
        with patch('src.scraper.Scraper.get_team_info') as mock_team_info:
            self.scraper = Scraper(self.logger)
        self.scraper.guessRightMatch = Mock()
        self.team1 = "home"
        self.team2 = "away"

    @patch('requests.get')
    def test_normal(self, mock_get):
        gameID = "000000"
        html = DummyMatchesHTML(self.team1, self.team2, gameid=gameID)
        mock_get.return_value = html
        found_id = self.scraper.findMatchSiteExact(self.team1)
        self.assertEqual(gameID, found_id)

    @patch('requests.get')
    def test_only_away(self, mock_get):
        gameID = "000000"
        html = DummyMatchesHTML(self.team1, self.team2, gameid=gameID)
        mock_get.return_value = html
        found_id = self.scraper.findMatchSiteExact(self.team2)
        self.assertEqual(gameID, found_id)

    @patch('requests.get')
    def test_missing_suffix(self, mock_get):
        """ Should not find the game. """
        gameID = "000000"
        html = DummyMatchesHTML(self.team1 + " United", self.team2, gameid=gameID)
        mock_get.return_value = html
        found_id = self.scraper.findMatchSiteExact(self.team1)
        self.assertEqual(ExceptionCodes.NO_MATCH, found_id)

    @patch('requests.get')
    def test_missing_prefix(self, mock_get):
        """ Should not find the game. """
        gameID = "000000"
        html = DummyMatchesHTML("FC " + self.team1, self.team2, gameid=gameID)
        mock_get.return_value = html
        found_id = self.scraper.findMatchSiteExact(self.team1)
        self.assertEqual(ExceptionCodes.NO_MATCH, found_id)

    @patch('requests.get')
    def test_missing_prefix_suffix(self, mock_get):
        """ Should not find the game. """
        gameID = "000000"
        html = DummyMatchesHTML("FC " + self.team1 + " Athletic", self.team2, gameid=gameID)
        mock_get.return_value = html
        found_id = self.scraper.findMatchSiteExact(self.team1)
        self.assertEqual(ExceptionCodes.NO_MATCH, found_id)

    @patch('requests.get')
    def test_not_there(self, mock_get):
        """ Should not find the game. """
        html = DummyMatchesHTML(self.team1, self.team2)
        mock_get.return_value = html
        found_id = self.scraper.findMatchSiteExact("team 1")
        self.assertEqual(ExceptionCodes.NO_MATCH, found_id)

    @patch('requests.get')
    def test_none(self, mock_get):
        """ Should not find the game. """
        html = DummyMatchesHTML(self.team1, self.team2)
        mock_get.return_value = html
        found_id = self.scraper.findMatchSiteExact(None)
        self.assertEqual(ExceptionCodes.NO_MATCH, found_id)

    @patch('requests.get')
    def test_selects_neither_home(self, mock_get):
        gameID = "000000"
        html = DummyMatchesHTML(self.team1 + " FC", self.team2, gameid=gameID)
        html.add_game(self.team1 + " Utd", self.team2, gameid="000000")
        mock_get.return_value = html
        found_id = self.scraper.findMatchSiteExact(self.team1)
        self.assertEqual(ExceptionCodes.NO_MATCH, found_id)

    @patch('requests.get')
    def test_selects_neither_away(self, mock_get):
        gameID = "000000"
        html = DummyMatchesHTML(self.team1, self.team2 + " FC", gameid=gameID)
        html.add_game(self.team1, self.team2 + " Utd", gameid="000000")
        mock_get.return_value = html
        found_id = self.scraper.findMatchSiteExact(self.team2)
        self.assertEqual(ExceptionCodes.NO_MATCH, found_id)

    @patch('requests.get')
    def test_selects_best_away_closer(self, mock_get):
        gameID = "000000"
        html = DummyMatchesHTML(self.team1, self.team2, gameid=gameID)
        html.add_game(self.team1, self.team2 + " Utd", gameid="000000")
        mock_get.return_value = html
        found_id = self.scraper.findMatchSiteExact(self.team2)
        self.assertEqual(gameID, found_id)

    @patch('requests.get')
    def test_selects_best_first(self, mock_get):
        gameID = "000000"
        html = DummyMatchesHTML(self.team1, self.team2, gameid=gameID)
        html.add_game("bad", "bad", "123456")
        mock_get.return_value = html
        found_id = self.scraper.findMatchSiteExact(self.team1)
        self.assertEqual(gameID, found_id)

    @patch('requests.get')
    def test_selects_best_second(self, mock_get):
        gameID = "000000"
        html = DummyMatchesHTML("bad", "bad", "123456")
        html.add_game(self.team1, self.team2, gameid=gameID)
        mock_get.return_value = html
        found_id = self.scraper.findMatchSiteExact(self.team1)
        self.assertEqual(gameID, found_id)

    @patch('requests.get')
    def test_selects_best_home_closer(self, mock_get):
        gameID = "000000"
        html = DummyMatchesHTML(self.team1, self.team2, gameid=gameID)
        html.add_game(self.team1 + " Utd", self.team2, gameid="000000")
        mock_get.return_value = html
        found_id = self.scraper.findMatchSiteExact(self.team1)
        self.assertEqual(gameID, found_id)

    @patch('requests.get')
    def test_selects_best_away_closer(self, mock_get):
        gameID = "000000"
        html = DummyMatchesHTML(self.team1, self.team2, gameid=gameID)
        html.add_game(self.team1, self.team2 + " Utd", gameid="000000")
        mock_get.return_value = html
        found_id = self.scraper.findMatchSiteExact(self.team2)
        self.assertEqual(gameID, found_id)





    @patch('requests.get')
    def test_timeout(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = requests.exceptions.Timeout(error_message)

        found_id = self.scraper.findMatchSiteExact(self.team1)

        self.logger.warning.assert_called_with(f"Requests Timeout in findMatchSiteExact. "
                                               f"<class 'requests.exceptions.Timeout'> - ('{error_message}',) "
                                               f"- {error_message}.")
        self.assertEqual(ExceptionCodes.RETRY, found_id)

    @patch('requests.get')
    def test_connection_error(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = requests.exceptions.ConnectionError(error_message)

        found_id = self.scraper.findMatchSiteExact(self.team1)

        self.logger.warning.assert_called_with(f"Requests Connection Error in findMatchSiteExact. "
                                               f"<class 'requests.exceptions.ConnectionError'> - ('{error_message}',) "
                                               f"- {error_message}.")
        self.assertEqual(ExceptionCodes.RETRY, found_id)

    @patch('requests.get')
    def test_requests_error(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = requests.exceptions.RequestException(error_message)
        found_id = self.scraper.findMatchSiteExact(self.team1)
        self.logger.warning.assert_called_with(f"Requests Unknown Error in findMatchSiteExact. "
                                               f"<class 'requests.exceptions.RequestException'> - ('{error_message}',) "
                                               f"- {error_message}.")
        self.assertEqual(ExceptionCodes.RETRY, found_id)

    @patch('requests.get')
    def test_chunked_error(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = requests.exceptions.ChunkedEncodingError(error_message)
        found_id = self.scraper.findMatchSiteExact(self.team1)
        self.logger.warning.assert_called_with(f"Requests Chunked Encoding Error in findMatchSiteExact. "
                                               f"<class 'requests.exceptions.ChunkedEncodingError'> - ('{error_message}',) "
                                               f"- {error_message}.")
        self.assertEqual(ExceptionCodes.RETRY, found_id)

    @patch('requests.get')
    def test_error(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = Exception(error_message)
        found_id = self.scraper.findMatchSiteExact(self.team1)
        self.logger.error.assert_called_with(f"Unknown Error in findMatchSiteExact. "
                                             f"<class 'Exception'> - ('{error_message}',) "
                                             f"- {error_message}.")
        self.assertEqual(ExceptionCodes.UNKNOWN_FAILURE, found_id)




