from unittest import TestCase
from unittest.mock import patch, mock_open, Mock
import sys
import pathlib
import os
import requests

from src.scraper import Scraper
from src.codes import *
from tests.DummyClasses import *


class GetStatusTests(TestCase):
    def setUp(self):
        self.logger = Mock()
        self.scraper = Scraper(self.logger)


class UpdateScoreTests(TestCase):
    def setUp(self):
        self.logger = Mock()
        self.scraper = Scraper(self.logger)


class GetTimesTests(TestCase):
    def setUp(self):
        self.logger = Mock()
        self.scraper = Scraper(self.logger)

