from unittest import TestCase
from unittest.mock import patch, mock_open, Mock
import sys
import pathlib
import os
import requests

from src.scraper import Scraper
from src.codes import *
from tests.DummyClasses import *


class GrabEventsTests(TestCase):
    def setUp(self):
        self.logger = Mock()
        with patch('src.scraper.Scraper.get_team_info') as mock_team_info:
            self.scraper = Scraper(self.logger)

    @patch('requests.get')
    def test_normal(self, mock_get):
        self.scraper.grabEvents("000000", "")
        self.assertEqual("0", '0')
