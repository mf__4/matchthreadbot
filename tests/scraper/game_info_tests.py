from unittest import TestCase
from unittest.mock import patch, mock_open, Mock
import sys
import pathlib
import os
import requests

from src.scraper import Scraper
from src.codes import *
from tests.DummyClasses import *

class GetExtraInfoTests(TestCase):
    def setUp(self):
        self.logger = Mock()
        with patch('src.scraper.Scraper.get_team_info') as mock_team_info:
            self.scraper = Scraper(self.logger)
        self.gameID = "000000"

    @patch('requests.get')
    def test_normal(self, mock_get):
        content_expected = "additional info"
        html = DummyGameHTML(provided_info=content_expected)
        mock_get.return_value = html
        found_content = self.scraper.getExtraInfo(self.gameID)
        self.assertEqual(content_expected, found_content)

    @patch('requests.get')
    def test_empty(self, mock_get):
        content_expected = ""
        html = DummyGameHTML(provided_info=content_expected)
        mock_get.return_value = html
        found_content = self.scraper.getExtraInfo(self.gameID)
        self.assertEqual(content_expected, found_content)

    @patch('requests.get')
    def test_doesnt_exists(self, mock_get):
        content_expected = ""
        html = DummyGameHTML(exists=False)
        mock_get.return_value = html
        found_content = self.scraper.getExtraInfo(self.gameID)
        self.assertEqual(content_expected, found_content)



    @patch('requests.get')
    def test_timeout(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = requests.exceptions.Timeout(error_message)

        found_id = self.scraper.getExtraInfo(self.gameID)

        self.logger.warning.assert_called_with(f"Requests Timeout in getExtraInfo. "
                                               f"<class 'requests.exceptions.Timeout'> - ('{error_message}',) "
                                               f"- {error_message}.")
        self.assertEqual(ExceptionCodes.RETRY, found_id)

    @patch('requests.get')
    def test_connection_error(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = requests.exceptions.ConnectionError(error_message)

        found_id = self.scraper.getExtraInfo(self.gameID)

        self.logger.warning.assert_called_with(f"Requests Connection Error in getExtraInfo. "
                                               f"<class 'requests.exceptions.ConnectionError'> - ('{error_message}',) "
                                               f"- {error_message}.")
        self.assertEqual(ExceptionCodes.RETRY, found_id)

    @patch('requests.get')
    def test_requests_error(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = requests.exceptions.RequestException(error_message)
        found_id = self.scraper.getExtraInfo(self.gameID)
        self.logger.warning.assert_called_with(f"Requests Unknown Error in getExtraInfo. "
                                               f"<class 'requests.exceptions.RequestException'> - ('{error_message}',) "
                                               f"- {error_message}.")
        self.assertEqual(ExceptionCodes.RETRY, found_id)

    @patch('requests.get')
    def test_chunked_error(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = requests.exceptions.ChunkedEncodingError(error_message)
        found_id = self.scraper.getExtraInfo(self.gameID)
        self.logger.warning.assert_called_with(f"Requests Chunked Encoding Error in getExtraInfo. "
                                               f"<class 'requests.exceptions.ChunkedEncodingError'> - ('{error_message}',) "
                                               f"- {error_message}.")
        self.assertEqual(ExceptionCodes.RETRY, found_id)

    @patch('requests.get')
    def test_error(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = Exception(error_message)
        found_id = self.scraper.getExtraInfo(self.gameID)
        self.logger.error.assert_called_with(f"Unknown Error in getExtraInfo. "
                                             f"<class 'Exception'> - ('{error_message}',) "
                                             f"- {error_message}.")
        self.assertEqual(ExceptionCodes.UNKNOWN_FAILURE, found_id)


class GetLineupsTests(TestCase):
    def setUp(self):
        self.logger = Mock()
        self.scraper = Scraper(self.logger)



class GetMatchInfoTests(TestCase):
    def setUp(self):
        self.logger = Mock()
        self.scraper = Scraper(self.logger)


