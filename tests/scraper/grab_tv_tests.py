from unittest import TestCase
from unittest.mock import patch, mock_open, Mock, call
import sys
import pathlib
import os
import requests
import datetime

from src.scraper import Scraper
from src.codes import *
from tests.DummyClasses import *


class GrabTV(TestCase):
    def setUp(self):
        self.logger = Mock()
        with patch('src.scraper.Scraper.get_team_info') as mock_team_info:
            self.scraper = Scraper(self.logger)

        self.t1 = "home"
        self.t2 = "away"
        self.standard_id = "123"

        today = datetime.date.today()
        self.today = self.create_time_string(today)

    def create_time_string(self, dt_obj):
        day = dt_obj.day
        if 4 <= day <= 20 or 24 <= day <= 30:
            suffix_today = "th"
        else:
            suffix_today = ["st", "nd", "rd"][day % 10 - 1]

        try:
            # linux method
            today_string = dt_obj.strftime(f"%A %-d{suffix_today} %B %Y")
        except ValueError:
            # windows method
            today_string = dt_obj.strftime(f"%A %#d{suffix_today} %B %Y")
        return today_string

    @patch('requests.get')
    def test_normal(self,  mock_get):
        games = {f"{self.t1} v {self.t2}": ("tv channel", self.today, "12:00", "Competition")}
        html = DummyTVHTML(games)
        mock_get.return_value = html

        expected_value = "**Watch live on tv channel**\n\n"

        end_value = self.scraper.grabTV(self.t1, self.t2)

        self.assertEqual(expected_value, end_value)

    @patch('requests.get')
    def test_backwards(self,  mock_get):
        games = {f"{self.t2} v {self.t1}": ("tv channel", self.today, "12:00", "Competition")}
        html = DummyTVHTML(games)
        mock_get.return_value = html

        expected_value = "**Watch live on tv channel**\n\n"

        end_value = self.scraper.grabTV(self.t1, self.t2)

        self.assertEqual(expected_value, end_value)

    @patch('requests.get')
    def test_vs_forwards(self,  mock_get):
        games = {f"{self.t1} vs {self.t2}": ("tv channel", self.today, "12:00", "Competition")}
        html = DummyTVHTML(games)
        mock_get.return_value = html

        expected_value = "**Watch live on tv channel**\n\n"

        end_value = self.scraper.grabTV(self.t1, self.t2)

        self.assertEqual(expected_value, end_value)

    @patch('requests.get')
    def test_vs_backwards(self,  mock_get):
        games = {f"{self.t2} vs {self.t1}": ("tv channel", self.today, "12:00", "Competition")}
        html = DummyTVHTML(games)
        mock_get.return_value = html

        expected_value = "**Watch live on tv channel**\n\n"

        end_value = self.scraper.grabTV(self.t1, self.t2)

        self.assertEqual(expected_value, end_value)

    @patch('requests.get')
    def test_not_there(self,  mock_get):
        games = {}
        html = DummyTVHTML(games)
        mock_get.return_value = html

        expected_value = ExceptionCodes.FAILURE

        end_value = self.scraper.grabTV(self.t1, self.t2)
        self.assertEqual(expected_value, end_value)
        self.logger.info.assert_called_with(f"No channel found for {self.t1} vs {self.t2}.")

    @patch('requests.get')
    def test_two_versions(self,  mock_get):
        """ Gets two copies of the same game, will check they match and select one """
        games = {f"{self.t1} v {self.t2}": ("tv channel", self.today, "12:00", "Competition"),
                 f"{self.t2} v {self.t1}": ("tv channel", self.today, "12:00", "Competition")}
        html = DummyTVHTML(games)
        mock_get.return_value = html

        expected_value = "**Watch live on tv channel**\n\n"

        end_value = self.scraper.grabTV(self.t1, self.t2)

        self.assertEqual(expected_value, end_value)

    @patch('requests.get')
    def test_checks_date(self,  mock_get):
        """ Actually looks at the date """
        tomorrow = datetime.date.today() + datetime.timedelta(days=1)
        tomorrow_string = self.create_time_string(tomorrow)

        games = {f"{self.t1} v {self.t2}": ("tv channel", tomorrow_string, "12:00", "Competition"),
                 f"{self.t2} v {self.t1}": ("tv channel", tomorrow_string, "12:00", "Competition")}
        html = DummyTVHTML(games)
        mock_get.return_value = html

        expected_value = ExceptionCodes.FAILURE

        end_value = self.scraper.grabTV(self.t1, self.t2)

        self.assertEqual(expected_value, end_value)

    @patch('requests.get')
    def test_checks_date_valid_invalid(self,  mock_get):
        """ Actually looks at the date """
        tomorrow = datetime.date.today() + datetime.timedelta(days=1)
        tomorrow_string = self.create_time_string(tomorrow)

        games = {f"random vs other": ("tv channel", self.today, "12:00", "Competition"),
                 f"{self.t1} v {self.t2}": ("tv channel", tomorrow_string, "12:00", "Competition")}
        html = DummyTVHTML(games)
        mock_get.return_value = html

        expected_value = ExceptionCodes.FAILURE

        end_value = self.scraper.grabTV(self.t1, self.t2)

        self.assertEqual(expected_value, end_value)

    @patch('requests.get')
    def test_ignores_future(self,  mock_get):
        """ Actually looks at the date """
        tomorrow = datetime.date.today() + datetime.timedelta(days=1)
        tomorrow_string = self.create_time_string(tomorrow)

        games = {f"{self.t1} v {self.t2}": ("tv channel", self.today, "12:00", "Competition")}
        html = DummyTVHTML(games)
        html.add_games({f"{self.t1} v {self.t2}": ("tv channel", tomorrow_string, "12:00", "Competition")})
        mock_get.return_value = html

        expected_value = "**Watch live on tv channel**\n\n"

        end_value = self.scraper.grabTV(self.t1, self.t2)

        self.assertEqual(expected_value, end_value)

    @patch('requests.get')
    def test_no_date(self,  mock_get):
        """ Handles no dates nicely """

        games = {f"{self.t1} v {self.t2}": ("tv channel", self.today, "12:00", "Competition")}
        html = DummyTVHTML(games)
        html.remove_div_class("fixture-date")

        mock_get.return_value = html

        expected_value = ExceptionCodes.FAILURE

        end_value = self.scraper.grabTV(self.t1, self.t2)

        self.assertEqual(expected_value, end_value)

    @patch('requests.get')
    def test_no_ko_time(self,  mock_get):
        """ Handles no ko time nicely """

        games = {f"{self.t1} v {self.t2}": ("tv channel", self.today, "12:00", "Competition")}
        html = DummyTVHTML(games)
        html.remove_div_class("fixture__time")

        mock_get.return_value = html

        expected_value = "**Watch live on tv channel**\n\n"

        end_value = self.scraper.grabTV(self.t1, self.t2)

        self.assertEqual(expected_value, end_value)


    @patch('requests.get')
    def test_no_tv_channel(self,  mock_get):
        """ Handles no tv channel nicely """

        games = {f"{self.t1} v {self.t2}": ("tv channel", self.today, "12:00", "Competition")}
        html = DummyTVHTML(games)
        html.remove_div_class("fixture__teams")

        mock_get.return_value = html

        expected_value = ExceptionCodes.FAILURE

        end_value = self.scraper.grabTV(self.t1, self.t2)

        self.assertEqual(expected_value, end_value)

    @patch('requests.get')
    def test_no_competition_info(self,  mock_get):
        """ Handles no competition info nicely """

        games = {f"{self.t1} v {self.t2}": ("tv channel", self.today, "12:00", "Competition")}
        html = DummyTVHTML(games)
        html.remove_div_class("fixture__competition")

        mock_get.return_value = html

        expected_value = "**Watch live on tv channel**\n\n"

        end_value = self.scraper.grabTV(self.t1, self.t2)

        self.assertEqual(expected_value, end_value)

    @patch('requests.get')
    def test_no_teams(self,  mock_get):
        """ Handles no team info nicely """

        games = {f"{self.t1} v {self.t2}": ("tv channel", self.today, "12:00", "Competition")}
        html = DummyTVHTML(games)
        html.remove_div_class("fixture__teams")

        mock_get.return_value = html

        expected_value = ExceptionCodes.FAILURE

        end_value = self.scraper.grabTV(self.t1, self.t2)

        self.assertEqual(expected_value, end_value)

    @patch('requests.get')
    def test_timeout(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = requests.exceptions.Timeout(error_message)

        found_id = self.scraper.grabTV(self.t1, self.t2)

        self.logger.warning.assert_called_with(f"Requests Timeout in getTVInfo. "
                                               f"<class 'requests.exceptions.Timeout'> - ('{error_message}',) "
                                               f"- {error_message}.")
        self.assertEqual(ExceptionCodes.RETRY, found_id)

    @patch('requests.get')
    def test_connection_error(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = requests.exceptions.ConnectionError(error_message)


        found_id = self.scraper.grabTV(self.t1, self.t2)

        self.logger.warning.assert_called_with(f"Requests Connection Error in getTVInfo. "
                                               f"<class 'requests.exceptions.ConnectionError'> - ('{error_message}',) "
                                               f"- {error_message}.")
        self.assertEqual(ExceptionCodes.RETRY, found_id)

    @patch('requests.get')
    def test_requests_error(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = requests.exceptions.RequestException(error_message)

        found_id = self.scraper.grabTV(self.t1, self.t2)
        self.logger.warning.assert_called_with(f"Requests Unknown Error in getTVInfo. "
                                               f"<class 'requests.exceptions.RequestException'> - ('{error_message}',) "
                                               f"- {error_message}.")
        self.assertEqual(ExceptionCodes.RETRY, found_id)

    @patch('requests.get')
    def test_chunked_error(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = requests.exceptions.ChunkedEncodingError(error_message)

        found_id = self.scraper.grabTV(self.t1, self.t2)
        self.logger.warning.assert_called_with(f"Requests Chunked Encoding Error in getTVInfo. "
                                               f"<class 'requests.exceptions.ChunkedEncodingError'> - ('{error_message}',) "
                                               f"- {error_message}.")
        self.assertEqual(ExceptionCodes.RETRY, found_id)

    @patch('requests.get')
    def test_error(self, mock_get):
        error_message = "oh no deary me"
        mock_get.side_effect = Exception(error_message)

        found_id = self.scraper.grabTV(self.t1, self.t2)
        self.logger.error.assert_called_with(f"Unknown Error in getTVInfo. "
                                             f"<class 'Exception'> - ('{error_message}',) "
                                             f"- {error_message}.")
        self.assertEqual(ExceptionCodes.UNKNOWN_FAILURE, found_id)

