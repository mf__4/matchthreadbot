import re, datetime, requests, requests.auth, unicodedata
from collections import Counter
from itertools import groupby
from bs4 import BeautifulSoup
import bs4
import os
from difflib import SequenceMatcher
from functools import lru_cache

from src.codes import *

# markup constants
goal=0;pgoal=1;ogoal=2;mpen=3;yel=4;syel=5;red=6;subst=7;subo=8;subi=9;strms=10;lines=11;evnts=12


def handle_exceptions(func):
    def handle(self, *args, **kwargs):
        try:
            return func(self, *args, **kwargs)
        except requests.exceptions.Timeout as exp:
            self.logger.warning(f"Requests Timeout in {func.__name__}. {type(exp)} - {exp.args} - {exp}.")
            return ExceptionCodes.RETRY
        except requests.exceptions.ConnectionError as exp:
            self.logger.warning(f"Requests Connection Error in {func.__name__}. {type(exp)} - {exp.args} - {exp}.")
            return ExceptionCodes.RETRY
        except requests.exceptions.ChunkedEncodingError as exp:
            self.logger.warning(f"Requests Chunked Encoding Error in {func.__name__}. {type(exp)} - {exp.args} - {exp}.")
            return ExceptionCodes.RETRY
        except requests.exceptions.RequestException as exp:
            self.logger.warning(f"Requests Unknown Error in {func.__name__}. {type(exp)} - {exp.args} - {exp}.")
            return ExceptionCodes.RETRY
        except ValueError as exp:
            self.logger.error(f"Value error in {func.__name__}. {type(exp)} - {exp.args} - {exp}.")
            return ExceptionCodes.FAILURE
        except Exception as exp:
            self.logger.error(f"Unknown Error in {func.__name__}. {type(exp)} - {exp.args} - {exp}.")
            return ExceptionCodes.UNKNOWN_FAILURE

    return handle


class Scraper:
    def __init__(self, logger, default_sub="scottishfootball"):
        self.logger = logger
        print("scraper")
        self.non_sidebar_subs = [u"scottishfootball", u"SFMatchThreads", u"weerovers"]
        self.teams_info = {}
        self.subs = self.get_team_info()
        self.max_retries = 5
        self.default_sub = default_sub

    def get_related_subs(self):
        return self.subs

    @handle_exceptions
    def get_team_info(self):
        """ Gets all subreddits from sidebar and their information """
        self.logger.info("Gathering related subreddits...")
        subs = []

        all_tables = []
        count = 0
        max_count = 50
        while not list(all_tables) and count < max_count:
            # try it 50 times, seems intermittent

            lineAddress = f"https://www.reddit.com/r/scottishfootball/"
            lineWebsite = requests.get(lineAddress, timeout=15, stream=True)
            line_html = lineWebsite.text
            html = BeautifulSoup(line_html, 'html.parser')

            all_tables = html.find_all(name="table")
            count += 1

        if not all_tables:
            self.logger.error(f"Tried {max_count} times to get the sidebar tables and couldn't. Reverting "
                              f"to hardcoded subreddits.")

            with open(os.path.join('bot_files', 'allowed_subs.txt')) as hardcoded_subs:
                all_lines = hardcoded_subs.readlines()

                all_lines = [l for l in all_lines if l]

                for team in all_lines:
                    subs.append(team.rstrip())

            subs.extend(self.non_sidebar_subs)
            subs = [x.lower() for x in subs]
            return subs
        else:
            self.logger.info(f"Took {count} attempt(s) to get the sidebar tables")

        for table in all_tables:
            table_content = table.find_all('tr')
            thead, *teams = tuple(table_content)

            # is this one of the league tables?
            header_row = thead.text.replace("\n", " ").strip()
            if not header_row == "# Team Pl GD Pt":
                self.logger.error(f"Header row has changed? Got in {header_row}.")
                continue

            for team in teams:
                if not isinstance(team, bs4.Tag):
                    continue

                info = team.text.split("\n")
                # remove any empty strings, make it a tuple for easier unpacking
                info = tuple([i.strip() for i in info if i])
                pos, name, pld, gd, pnt = info
                sub = None

                links = team.find_all("a")
                for link in links:
                    subreddit_finder = re.findall('.*?https://www.reddit.com/r/(.*?)/.*?', link['href'])
                    if subreddit_finder:
                        sub = subreddit_finder[0]
                        subs.append(sub)
                        break

                self.teams_info[name] = (pos, pld, gd, pnt, sub)
                self.logger.info(f"Found team {name} in the sidebar, "
                                 f"subreddit: {[sub if sub is not None else 'not found'][0].lower()}. "
                                 f"They are in {pos} position in their league "
                                 f"after {pld} games, {gd} goal difference and {pnt} points.")

        if not subs:
            # no subreddits found.
            with open(os.path.join('bot_files', 'allowed_subs.txt')) as hardcoded_subs:
                all_lines = hardcoded_subs.readlines()

                all_lines = [l for l in all_lines if l.rstrip()]

                for team in all_lines:
                    subs.append(team.rstrip())

        # main sub
        subs.extend(self.non_sidebar_subs)
        subs = [x.lower() for x in subs]
        return subs

    def remove_accents(self, input_str):
        nfkd_form = unicodedata.normalize('NFKD', input_str)
        return u"".join([c for c in nfkd_form if not unicodedata.combining(c)])

    @handle_exceptions
    @lru_cache(maxsize=12)
    def findBBCSiteSingle(self, team1, team2):
        # search for each word in each team name in the fixture list, return most frequent result
        self.logger.info("Finding BBC site for {} vs {}...".format(team1, team2))
        allTeams = {}

        fixAddress = "https://www.bbc.co.uk/sport/football/scores-fixtures"
        fixWebsite = requests.get(fixAddress, timeout=15, stream=True)
        fix_html = fixWebsite.text


        html = BeautifulSoup(fix_html, 'html.parser')


        possible = {}

        all_leagues = html.find_all("div", {"class": "qa-match-block"})
        for league in all_leagues:
            games = league.find_all('li')
            for game in games:
                if "Match postponed".lower() in game.text.lower():
                    self.logger.debug(f"{team1} vs {team2} is postponed.")
                    continue
                teams = game.find_all("span", {"class": 'sp-c-fixture__team-name-wrap'})

                if len(teams) != 2:
                    self.logger.error(f"BBC page doesn't have exactly two teams, {len(teams)} team(s) found.")
                    continue

                home = teams[0].find("span")
                away = teams[1].find("span")

                if home is None or away is None:
                    self.logger.error(f"BBC PAGE ERROR. HOME OR AWAY TEAM FOR {team1} vs {team2} IS NONE")
                    continue

                link = game.find_all("a", href=True)
                if len(link) > 1:
                    self.logger.error(f"MORE THAN ONE LINK ON BBC PAGE FOR {home.text} vs {away.text}")
                    continue
                elif len(link) > 0:
                    link = link[0]
                else:
                    self.logger.debug(f"NO LINK ON BBC PAGE FOR {home.text} vs {away.text}")
                    continue
                match_id = re.findall("(?:[^\d]*)([\d]*)", link['href'])

                valid_ids = []
                #remove empty strings
                for match in match_id:
                    if match != '':
                        valid_ids.append(match)

                if len(valid_ids) > 1:
                    self.logger.error(f"MORE THAN ONE MATCHID ON BBC PAGE FOR {home.text} vs {away.text}")
                    continue
                else:
                    match_id = valid_ids[0]



                allTeams[match_id] = (home.text, away.text)

                t1 = home.text.split()
                t2 = away.text.split()

                for word in t1:
                    if self.remove_accents(team1.lower()).find(self.remove_accents(word.lower())) != -1:
                        possible[match_id] = (home.text, away.text)
                    if self.remove_accents(team2.lower()).find(self.remove_accents(word.lower())) != -1:
                        possible[match_id] = (home.text, away.text)

                for word in t2:
                    if self.remove_accents(team1.lower()).find(self.remove_accents(word.lower())) != -1:
                        possible[match_id] = (home.text, away.text)
                    if self.remove_accents(team2.lower()).find(self.remove_accents(word.lower())) != -1:
                        possible[match_id] = (home.text, away.text)

        guess = self.guessRightMatch(possible, (team1, team2))
        if guess is None:
            return ExceptionCodes.NO_MATCH, "", "", ""
        h, a = possible[guess]
        return ExceptionCodes.SUCCESS, guess, h, a

    @handle_exceptions
    def get_BBC_page(self, bbc_id):
        if bbc_id == -1:
            self.logger.warning(f"FAILED TO GET STATS FOR BBC PAGE {bbc_id}")
            return ""
        lineAddress = "https://www.bbc.co.uk/sport/live/football/{}".format(bbc_id)
        request = requests.get(lineAddress, timeout=15, stream=True)

        # will happen if game isnt live anymore
        if request.status_code == 404:
            lineAddress = "https://www.bbc.co.uk/sport/football/{}".format(bbc_id)
            request = requests.get(lineAddress, timeout=15, stream=True)

        return request, lineAddress

    @handle_exceptions
    def grabStats(self, tt1, tt2):
        code = ExceptionCodes.FAILURE  # will get overwritten, just incase that somehow fails so it exits out
        bbc_id = ExceptionCodes.FAILURE
        t1, t2 = tt1, tt2
        # try it a couple of times in case a timeout or connection error etc happens.
        for i in range(self.max_retries):
            self.logger.info(f"Trying to get the BBC site. On attempt {i+1} out of {self.max_retries}")
            code, bbc_id, t1, t2 = self.findBBCSiteSingle(tt1, tt2)
            if code != ExceptionCodes.RETRY:
                # stop tring now it was a success
                break

        if code != ExceptionCodes.SUCCESS or bbc_id == ExceptionCodes.FAILURE:
            self.logger.info(f"No BBC Site found for {tt1} vs {tt2}.")
            return ExceptionCodes.FAILURE, ""

        request, url = self.get_BBC_page(bbc_id)

        line_html = request.text

        if request.status_code == 200:
            body = '\n\n---------\n\n'
            body += f'**MATCH STATS** | *via [BBC]({url})*\n\n'
            body += f"||{t1}|{t2}|\n|:--|:--:|:--:|\n"
            data = []
            html = BeautifulSoup(line_html, 'html.parser')


            match_stats = html.find("div", {"class": "sp-c-football-match-stats"})
            if match_stats is None:
                self.logger.debug("Stats arent available yet. Usually appear 5 mins in")
                return ExceptionCodes.FAILURE, ""

            match_stats_table = match_stats.find('dl')
            if match_stats_table is None:
                self.logger.debug("Stats arent available yet. Usually appear 5 mins in")
                return ExceptionCodes.FAILURE, ""

            rows = match_stats_table.find_all('dl')
            if rows is None:
                self.logger.debug("Stats arent available yet. Usually appear 5 mins in")
                return ExceptionCodes.FAILURE, ""

            for row in rows:
                heading = row.find("dt").text


                numbers = re.findall("[^\d]*(\d+)[^\d]*(\d+).*", row.text)
                if len(numbers) == 0:
                    # couldnt find two numbers, ignore this row
                    self.logger.warning(f"Could not find two numbers in a row. Row looks like this \"{row.text.strip()}\"")
                    continue
                home, away = numbers[0]
                # give possession a %
                if heading.lower() == "Possession".lower():
                    home = home + "%"
                    away = away + "%"
                data.append([heading, home, away])

            if len(data) == 0:
                self.logger.warning(f"Managed to find a stats table for {t1} vs {t2} but there was no data.")
                return ExceptionCodes.FAILURE, ""

            for d in data:
                body += "|"+d[0]+"|"+d[1]+"|"+d[2]+"|\n"

            # print_text("complete.")
            return ExceptionCodes.SUCCESS, body

        else:
            self.logger.warning("STATS EDIT BAD WEBPAGE STATUS CODE")
            return ExceptionCodes.FAILURE, ""

    @handle_exceptions
    def getTVInfo(self):
        fixAddress = "https://www.live-footballontv.com/live-scottish-football-on-tv.html"
        self.logger.info("Gathering TV Channel info")
        # Format today's date
        today = datetime.date.today()
        day = today.day
        if 4 <= day <= 20 or 24 <= day <= 30:
            suffix_today = "th"
        else:
            suffix_today = ["st", "nd", "rd"][day % 10 - 1]

        try:
            # linux method
            today_string = today.strftime(f"%A %-d{suffix_today} %B %Y")
        except ValueError:
            # windows method
            today_string = today.strftime(f"%A %#d{suffix_today} %B %Y")

        fixWebsite = requests.get(fixAddress, timeout=15, stream=True)
        fix_html = fixWebsite.text

        html = BeautifulSoup(fix_html, 'html.parser')
        possible = {}
        container = html.find_all("div", {"class": ["fixture", "fixture-date"]})
        valid = False
        for con in container:
            # looking for date
            if con.attrs["class"] == ["fixture-date"]:
                # date row
                if con.text == today_string:
                    valid = True
                else:
                    valid = False
                # dont for teams
                continue
            if valid:
                game = con.find("div", {"class", "fixture__teams"})
                tv_channel = con.find_all("span", {"class", "channel-pill"})
                if game is not None and tv_channel is not None:
                    # print("{} is on {}".format(game.text.strip(), tv_channel.text.strip()))
                    self.logger.info(f"{game.text.strip()} is on {tv_channel}")

                    # tidy up array
                    tv_channel = [i.text.strip() for i in tv_channel]
                    fix_team_names = {
                        # live-footballontv: espn

                        ## premiership
                        "Aberdeen": "Aberdeen",
                        "Celtic": "Celtic",
                        "Dundee": "Dundee",
                        "Dundee United": "Dundee United",
                        "Hearts": "Heart of Midlothian",
                        "Hibernian": "Hibernian",
                        "Livingston": "Livingston",
                        "Motherwell": "Motherwell",
                        "Rangers": "Rangers",
                        "Ross County": "Ross County",
                        "St Johnstone": "St Johnstone",
                        "St Mirren": "St Mirren",

                        ## championship
                        "Arbroath": "Arbroath",
                        "Ayr United": "Ayr United",
                        "Dunfermline": "Dunfermline Athletic",
                        "Greenock Morton": "Greenock Morton",
                        "Hamilton Academical": "Hamilton Academical",
                        "Inverness CT": "Inverness Caledonian Thistle",
                        "Kilmarnock": "Kilmarnock",
                        "Partick Thistle": "Partick Thistle",
                        "Raith Rovers": "Raith Rovers",
                        "Queen of the South": "Queen of the South"
                    }
                    game = game.text.strip()
                    for live, espn in fix_team_names.items():
                        game = game.replace(live, espn)

                    possible[game] = tv_channel
        return possible

    def grabTV(self, t1, t2):
        self.logger.info(f"Finding TV Channel for {t1} vs {t2}...")
        body = "**Watch live on "
        possible = self.getTVInfo()
        if type(possible) != dict:
            return possible

        count = 0
        channel = []
        if len(possible.keys()) > 0:
            # games found
            expected_fixture_1 = f"{t1} v {t2}"
            expected_fixture_2 = f"{t2} v {t1}"
            expected_fixture_3 = f"{t1} vs {t2}"
            expected_fixture_4 = f"{t2} vs {t1}"
            fixtures = [expected_fixture_1, expected_fixture_2, expected_fixture_3, expected_fixture_4]
            for teams in possible.keys():
                if teams in fixtures:
                    if count > 0 and channel == possible[teams]:
                        continue
                    channel = possible[teams]
                    self.logger.info(f"Found matching game for {t1} v {t2} ({teams} on {channel})")
                    break

        if len(channel) == 0:
            self.logger.info(f"No channel found for {t1} vs {t2}.")
            return ExceptionCodes.FAILURE

        for ch in channel:
            # auto link iplayer
            if ch == "BBC Alba":
                ch = "[BBC Alba](https://www.bbc.co.uk/iplayer/live/bbcalba)"
            if ch == "BBC Scotland":
                ch = "[BBC Scotland](https://www.bbc.co.uk/iplayer/live/bbcscotland)"
            body += ch + " or "
        body = body[:-4] + "**\n\n"
        count += 1

        return body

    def getBBCGames(self):
        possible = self.getTVInfo()

        for game, channel in possible.items():
            for ch in channel:
                if ch == "BBC iPlayer":
                    return game
        return None


    def grabEvents(self, matchID, markup):
        lineAddress = f"http://www.espn.com/soccer/commentary?gameId={matchID}"

        lineWebsite = requests.get(lineAddress, timeout=15, stream=True)
        line_html = lineWebsite.text
        try:
            if lineWebsite.status_code == 200:
                body = ""
                split_all = line_html.split('<h1>Match Commentary</h1>')  # [0]:stuff [1]:commentary + key events
                split = split_all[1].split('<h1>Key Events</h1>')  # [0]:commentary [1]: key events

                events = re.findall('<tr data-id=(.*?)</tr>', split[1], re.DOTALL)
                events = events[::-1]

                # will only report goals (+ penalties, own goals), yellows, reds, subs
                supportedEvents = ['goal', 'goal---header', 'goal---free-kick', 'penalty---scored', 'own-goal',
                                   'penalty---missed', 'penalty---saved', 'yellow-card', 'red-card', 'substitution']
                for text in events:
                    tag = re.findall('data-type="(.*?)"', text, re.DOTALL)[0]
                    if tag.lower() in supportedEvents:
                        time = re.findall('"time-stamp">(.*?)<', text, re.DOTALL)[0]
                        time = time.strip()
                        info = "**" + time + "** "
                        event = re.findall('"game-details">(.*?)</td', text, re.DOTALL)[0].strip()
                        if tag.lower().startswith('goal') or tag.lower() == 'penalty---scored' or tag.lower() == 'own-goal':
                            if tag.lower().startswith('goal'):
                                info += markup[goal] + ' **' + event + '**'
                            elif tag.lower() == 'penalty---scored':
                                info += markup[pgoal] + ' **' + event + '**'
                            else:
                                info += markup[ogoal] + ' **' + event + '**'
                        if tag.lower() == 'penalty---missed' or tag.lower() == 'penalty---saved':
                            info += markup[mpen] + ' **' + event + '**'
                        if tag.lower() == 'yellow-card':
                            info += markup[yel] + ' ' + event
                        if tag.lower() == 'red-card':
                            info += markup[red] + ' ' + event
                        if tag.lower() == 'substitution':
                            info += markup[subst] + ' ' + re.sub('<.*?>', '', event)
                        body += info + '\n\n'

                self.logger.debug("GRAB EVENTS COMPLETED")
                return body

            else:
                self.logger.warning("GRAB EVENTS BAD WEBPAGE STATUS CODE")
                return ""
        except:
            self.logger.warning("GRAB EVENTS FAILED")
            return ""

    def getTimes(self, ko):
        if ko.count(":") > 1:
            return ExceptionCodes.FAILURE, ExceptionCodes.FAILURE
        elif ko.count(":") == 1:
            hour = ko[0:ko.index(':')]
            minute = ko[ko.index(':') + 1:ko.index(':') + 3]
            try:
                hour_i = int(hour)
            except ValueError:
                hour_i = ExceptionCodes.FAILURE

            try:
                min_i = int(minute)
            except ValueError:
                min_i = ExceptionCodes.FAILURE

            return hour_i, min_i
        else:
            try:
                hour_i = int(ko)
                return hour_i, 0  # assume if only hour is given, its on the hour
            except ValueError:
                return ExceptionCodes.FAILURE, ExceptionCodes.FAILURE


    def getLineUps(self, matchID):
        try:
            # try to find line-ups
            lineAddress = f"http://www.espn.com/soccer/lineups?gameId={matchID}"
            lineWebsite = requests.get(lineAddress, timeout=15, stream=True)
            line_html = lineWebsite.text
            split = line_html.split('<div class="sub-module soccer">')  # [0]:nonsense [1]:team1 [2]:team2

            if len(split) > 1:
                team1StartBlock = split[1].split('Substitutes')[0]
                if len(split[1].split('Substitutes')) > 1:
                    team1SubBlock = split[1].split('Substitutes')[1]
                else:
                    team1SubBlock = ''
                team2StartBlock = split[2].split('Substitutes')[0]
                if len(split[2].split('Substitutes')) > 1:
                    team2SubBlock = split[2].split('Substitutes')[1]
                else:
                    team2SubBlock = ''

                team1Start = []
                team2Start = []
                team1Sub = []
                team2Sub = []

                t1StartInfo = re.findall('"accordion-item" data-id="(.*?)</div>', team1StartBlock, re.DOTALL)
                t1SubInfo = re.findall('"accordion-item" data-id="(.*?)</div>', team1SubBlock, re.DOTALL)
                t2StartInfo = re.findall('"accordion-item" data-id="(.*?)</div>', team2StartBlock, re.DOTALL)
                t2SubInfo = re.findall('"accordion-item" data-id="(.*?)</div>', team2SubBlock, re.DOTALL)

                for playerInfo in t1StartInfo:
                    playerInfo = playerInfo.replace('\t', '').replace('\n', '')
                    playerNum = playerInfo[0:6]
                    if '%' not in playerNum:
                        playertext = ''
                        if 'icon-soccer-substitution-before' in playerInfo:
                            playertext += '!sub '
                        playertext += re.findall('<span class="name">.*?data-player-uid=.*?>(.*?)<', playerInfo, re.DOTALL)[
                            0]
                        team1Start.append(playertext)
                for playerInfo in t1SubInfo:
                    playerInfo = playerInfo.replace('\t', '').replace('\n', '')
                    playerNum = playerInfo[0:6]
                    if '%' not in playerNum:
                        playertext = ''
                        playertext += re.findall('<span class="name">.*?data-player-uid=.*?>(.*?)<', playerInfo, re.DOTALL)[
                            0]
                        team1Sub.append(playertext)
                for playerInfo in t2StartInfo:
                    playerInfo = playerInfo.replace('\t', '').replace('\n', '')
                    playerNum = playerInfo[0:6]
                    if '%' not in playerNum:
                        playertext = ''
                        if 'icon-soccer-substitution-before' in playerInfo:
                            playertext += '!sub '
                        playertext += re.findall('<span class="name">.*?data-player-uid=.*?>(.*?)<', playerInfo, re.DOTALL)[
                            0]
                        team2Start.append(playertext)
                for playerInfo in t2SubInfo:
                    playerInfo = playerInfo.replace('\t', '').replace('\n', '')
                    playerNum = playerInfo[0:6]
                    if '%' not in playerNum:
                        playertext = ''
                        playertext += re.findall('<span class="name">.*?data-player-uid=.*?>(.*?)<', playerInfo, re.DOTALL)[
                            0]
                        team2Sub.append(playertext)

                # if no players found:
                if team1Start == []:
                    team1Start = ["*Not available*"]
                if team1Sub == []:
                    team1Sub = ["*Not available*"]
                if team2Start == []:
                    team2Start = ["*Not available*"]
                if team2Sub == []:
                    team2Sub = ["*Not available*"]
                return team1Start, team1Sub, team2Start, team2Sub

            else:
                team1Start = ["*Not available*"]
                team1Sub = ["*Not available*"]
                team2Start = ["*Not available*"]
                team2Sub = ["*Not available*"]
                return team1Start, team1Sub, team2Start, team2Sub
        except IndexError:
            self.logger.warning("INDEX ERROR")
            team1Start = ["*Not available*"]
            team1Sub = ["*Not available*"]
            team2Start = ["*Not available*"]
            team2Sub = ["*Not available*"]
            return team1Start, team1Sub, team2Start, team2Sub

        # get current match time/status

    def getStatus(self, matchID):
        lineAddress = f"http://www.espn.com/soccer/match?gameId={matchID}"
        lineWebsite = requests.get(lineAddress, timeout=15, stream=True)
        line_html = lineWebsite.text
        if lineWebsite.status_code == 200:
            status = re.findall('<span class="game-time".*?>(.*?)<',line_html,re.DOTALL)
            if status == []:
                return 'v'
            else:
                return status[0]
        else:
            return ''

    def getTeamIDs(self, matchID):
        try:
            lineAddress = f"http://www.espn.com/soccer/match?gameId={matchID}"
            lineWebsite = requests.get(lineAddress, timeout=15, stream=True)
            line_html = lineWebsite.text

            teamIDs = re.findall('<div class="team-info">(.*?)</div>', line_html, re.DOTALL)
            if teamIDs != []:
                t1id = re.findall('/(?:club|team)/.*?/.*?/(.*?)"', teamIDs[0], re.DOTALL)
                t2id = re.findall('/(?:club|team)/.*?/.*?/(.*?)"', teamIDs[1], re.DOTALL)
                if t1id != []:
                    t1id = t1id[0]
                else:
                    t1id = ''
                if t2id != []:
                    t2id = t2id[0]
                else:
                    t2id = ''
                return t1id, t2id
            else:
                return '', ''
        except requests.exceptions.Timeout:
            return '', ''

    @handle_exceptions
    def findMatchSite(self, team1, team2):
        # search for each word in each team name in the fixture list, return most frequent result
        if team1 is None:
            team1 = ""
        if team2 is None:
            team2 = ""

        if team1 is "" and team2 is "":
            return ExceptionCodes.NO_MATCH

        self.logger.debug(f"Finding ESPN site for {team1} vs {team2}...")


        t1 = team1.split()
        t2 = team2.split()
        games = {}
        fixAddress = "http://www.espn.com/soccer/scoreboard"
        fixWebsite = requests.get(fixAddress, timeout=15, stream=True)
        fix_html = fixWebsite.text
        matches = fix_html.split('window.espn.scoreboardData')[1]
        matches = matches.split('<body class="scoreboard')[0]
        names = matches.split('"text":"Statistics"')
        del names[-1]
        for match in names:
            check = True
            matchID = re.findall('"homeAway":.*?"href":".*?gameId=(.*?)",', match, re.DOTALL)[0][0:6]

            homeTeam = re.findall('"homeAway":"home".*?"team":{.*?"alternateColor".*?"displayName":"(.*?)"', match, re.DOTALL)
            if len(homeTeam) > 0:
                homeTeam = homeTeam[0]
            else:
                check = False
            awayTeam = re.findall('"homeAway":"away".*?"team":{.*?"alternateColor".*?"displayName":"(.*?)"', match, re.DOTALL)
            if len(awayTeam) > 0:
                awayTeam = awayTeam[0]
            else:
                check = False
            if check:
                for word in t1:
                    if self.remove_accents(homeTeam.lower()).find(self.remove_accents(word.lower())) != -1:
                        games[matchID] = (homeTeam, awayTeam)
                    if self.remove_accents(awayTeam.lower()).find(self.remove_accents(word.lower())) != -1:
                        games[matchID] = (homeTeam, awayTeam)
                for word in t2:
                    if self.remove_accents(homeTeam.lower()).find(self.remove_accents(word.lower())) != -1:
                        games[matchID] = (homeTeam, awayTeam)
                    if self.remove_accents(awayTeam.lower()).find(self.remove_accents(word.lower())) != -1:
                        games[matchID] = (homeTeam, awayTeam)

        if len(games) > 0:
            guess = self.guessRightMatch(games, (team1, team2))
            self.logger.debug(f"Found match for {team1} vs {team2}.")
            if guess is None:
                return ExceptionCodes.NO_MATCH
            return guess
        else:
            self.logger.warning(f"Cannot find match for {team1} vs {team2}.")
            return ExceptionCodes.NO_MATCH

    # get venue, ref, lineups, etc from ESPN
    def getMatchInfo(self, matchID):
        lineAddress = f"http://www.espn.com/soccer/match?gameId={matchID}"
        self.logger.debug(f"Finding ESPN info from {lineAddress}...")
        lineWebsite = requests.get(lineAddress, timeout=15, stream=True)
        line_html = lineWebsite.text

        # get "fixed" versions of team names (ie team names from ESPNFC, not team names from match thread request)
        team1fix = re.findall('<span class="long-name">(.*?)<', line_html, re.DOTALL)[0]
        team2fix = re.findall('<span class="long-name">(.*?)<', line_html, re.DOTALL)[1]
        t1id, t2id = self.getTeamIDs(matchID)
        t1abb, t2abb = self.getTeamAbbrevs(matchID)

        if team1fix[-1] == ' ':
            team1fix = team1fix[0:-1]
        if team2fix[-1] == ' ':
            team2fix = team2fix[0:-1]

        status = self.getStatus(matchID)
        ko_date = re.findall('<span data-date="(.*?)T', line_html, re.DOTALL)
        if ko_date != []:
            ko_date = ko_date[0]
            ko_day = ko_date[8:]
            ko_time = re.findall('<span data-date=".*?T(.*?)Z', line_html, re.DOTALL)[0]
        # above time is actually 5 hours from now (ESPN time in source code)
        else:
            ko_day = ''
            ko_time = ''

        venue = re.findall('<div>VENUE: (.*?)<', line_html, re.DOTALL)
        if venue != []:
            venue = venue[0]
        else:
            venue = '?'

        compfull = re.findall('<div class="game-details header">(.*?)<', line_html, re.DOTALL)
        if compfull != []:
            comp = re.sub('20.*? ', '', compfull[0]).strip(' \n\t\r')
            if comp.find(',') != -1:
                comp = comp[0:comp.index(',')]
        else:
            comp = ''

        team1Start, team1Sub, team2Start, team2Sub = self.getLineUps(matchID)
        self.logger.debug(f"Match info gathered for {t1abb} vs {t2abb}.")
        return (
            team1fix, t1id, team2fix, t2id, team1Start, team1Sub, team2Start, team2Sub, venue, ko_day, ko_time, status, comp,
            t1abb, t2abb)

    @lru_cache(maxsize=12)
    def getTeamAbbrevs(self, matchID):
        lineAddress = f"http://www.espn.com/soccer/match?gameId={matchID}"
        lineWebsite = requests.get(lineAddress, timeout=15, stream=True)
        line_html = lineWebsite.text

        t1abb = re.findall('<span class="long-name">(.*?)<', line_html, re.DOTALL)[0][0:3].upper()
        t2abb = re.findall('<span class="long-name">(.*?)<', line_html, re.DOTALL)[1][0:3].upper()

        teamabbs = re.findall('<span class="abbrev">(.*?)<', line_html, re.DOTALL)
        if len(teamabbs) >= 2:
            t1abb = teamabbs[0][0:3]
            t2abb = teamabbs[1][0:3]
        return t1abb, t2abb

    @handle_exceptions
    def getExtraInfo(self, matchID):
        lineAddress = f"http://www.espn.com/soccer/match?gameId={matchID}"
        lineWebsite = requests.get(lineAddress, timeout=15, stream=True)
        line_html = lineWebsite.text
        info = re.findall('data-stat="note">(.*?)<',line_html,re.DOTALL)
        if info == []:
            return ''
        else:
            return info[0]

    def get_score(self, line_html=None, match_id=None):
        try:
            if line_html is None:
                if match_id is None:
                    return -1, -1
                line_address = f"http://www.espn.com/soccer/match?gameId={match_id}"
                line_website = requests.get(line_address, timeout=15, stream=True)
                line_html = line_website.text

            left_score = re.findall('data-stat="score">(.*?)<', line_html, re.DOTALL)[0].strip()
            right_score = re.findall('data-stat="score">(.*?)<', line_html, re.DOTALL)[1].strip()
            return left_score, right_score
        except requests.exceptions.Timeout:
            return -1, -1

    def get_goal_scorers(self, line_html=None, match_id=None):
        try:
            if line_html is None:
                if match_id is None:
                    return -1
                line_address = f"http://www.espn.com/soccer/match?gameId={match_id}"
                line_website = requests.get(line_address, timeout=15, stream=True)
                line_html = line_website.text

            left_info = re.findall('<div class="team-info players"(.*?)</div>', line_html, re.DOTALL)[0]
            right_info = re.findall('<div class="team-info players"(.*?)</div>', line_html, re.DOTALL)[1]

            left_goals = re.findall('data-event-type="goal"(.*?)</ul>', left_info, re.DOTALL)
            right_goals = re.findall('data-event-type="goal"(.*?)</ul>', right_info, re.DOTALL)

            if left_goals != []:
                left_scorers = re.findall('<li>(.*?)</li', left_goals[0], re.DOTALL)
            else:
                left_scorers = []

            if right_goals != []:
                right_scorers = re.findall('<li>(.*?)</li', right_goals[0], re.DOTALL)
            else:
                right_scorers = []

            return left_scorers, right_scorers
        except requests.exceptions.Timeout:
            return [], []

    # update score, scorers
    def updateScore(self, matchID, t1, t2, sub):
        try:
            line_address = f"http://www.espn.com/soccer/match?gameId={matchID}"
            line_website = requests.get(line_address, timeout=15, stream=True)
            line_html = line_website.text
            left_score, right_score = self.get_score(line_html)

            info = self.getExtraInfo(matchID)
            status = self.getStatus(matchID)
            espn_updating = True
            if status == 'v':
                status = "0'"
                espn_updating = False

            left_scorers, right_scorers = self.get_goal_scorers(line_html)

            text = '#**' + status + ": " + t1 + ' ' + left_score + '-' + right_score + ' ' + t2 + '**\n\n'

            if not espn_updating:
                text += '*If the match has started, ESPN might not be providing updates for this game.*\n\n'

            if info != '':
                text += '***' + info + '***\n\n'

            left = ''
            if left_scorers != []:
                left += "*" + t1 + " scorers: "
                for scorer in left_scorers:
                    scorer = scorer[0:scorer.index('<')].strip(' \t\n\r') + ' ' + scorer[scorer.index('('):scorer.index(
                        '/') - 1].strip(' \t\n\r')
                    left += scorer + ", "
                left = left[0:-2] + "*"

            right = ''
            if right_scorers != []:
                right += "*" + t2 + " scorers: "
                for scorer in right_scorers:
                    scorer = scorer[0:scorer.index('<')].strip(' \t\n\r') + ' ' + scorer[scorer.index('('):scorer.index(
                        '/') - 1].strip(' \t\n\r')
                    right += scorer + ", "
                right = right[0:-2] + "*"

            text += left + '\n\n' + right

            return text
        except requests.exceptions.Timeout:
            return '#**--**\n\n'

    @handle_exceptions
    def findMatchSiteExact(self, team):
        """ Find the exact match for a team. This is not accessible from reddit messages
            and will only allow exact matches to an ESPN name"""
        # search for each word in each team name in the fixture list, return most frequent result

        if team is None or team == "":
            return ExceptionCodes.NO_MATCH

        self.logger.debug(f"Finding ESPN site for {team}...")

        t1 = team.split()
        linkList = []
        fixAddress = "http://www.espn.com/soccer/scoreboard"
        fixWebsite = requests.get(fixAddress, timeout=15, stream=True)
        fix_html = fixWebsite.text
        matches = fix_html.split('window.espn.scoreboardData')[1]
        matches = matches.split('<body class="scoreboard')[0]
        names = matches.split('"text":"Statistics"')
        del names[-1]
        for match in names:
            check = True
            matchID = re.findall('"homeAway":.*?"href":".*?gameId=(.*?)",', match, re.DOTALL)[0][0:6]
            homeTeam = re.findall('"homeAway":"home".*?"team":{.*?"alternateColor".*?"displayName":"(.*?)"', match, re.DOTALL)
            if len(homeTeam) > 0:
                homeTeam = homeTeam[0]
            else:
                check = False
            awayTeam = re.findall('"homeAway":"away".*?"team":{.*?"alternateColor".*?"displayName":"(.*?)"', match, re.DOTALL)
            if len(awayTeam) > 0:
                awayTeam = awayTeam[0]
            else:
                check = False
            if check:

                if self.remove_accents(homeTeam.lower()) == (self.remove_accents(team.lower())):
                    linkList.append(matchID)
                if self.remove_accents(awayTeam.lower()) == (self.remove_accents(team.lower())):
                    linkList.append(matchID)

        counts = Counter(linkList)
        if counts.most_common(1) != []:
            possibles = []
            for val, grp in groupby(counts.most_common(), lambda x: x[0]):
                possibles.append(val)
                if len(possibles) > 1:
                    mode = self.guessRightMatch(possibles)
                else:
                    mode = possibles[0]
            self.logger.info(f"Found match for {team}")
            return mode
        else:
            self.logger.info(f"No match found for {team}")
            return ExceptionCodes.NO_MATCH

    @handle_exceptions
    def guessRightMatch(self, possibles, actual):
        """
        Will attempt to choose the correct match. Will not choose anything less than 50% accurate
        :param possibles: dict consists of {matchID: (home, away), ...}
        :param actual: user provided home and away teams
        :return: closest guess
        """
        goal_home, goal_away = actual

        # guess first item, assume no similarity
        guess = None
        max_similarity = 0.0
        for match, teams in possibles.items():
            home, away = teams
            home_val = SequenceMatcher(None, home, goal_home).ratio()
            away_val = SequenceMatcher(None, away, goal_away).ratio()

            total_similarity = home_val + away_val

            # switch home and away around just incase
            o_home_val = SequenceMatcher(None, away, goal_home).ratio()
            o_away_val = SequenceMatcher(None, home, goal_away).ratio()
            o_total_similarity = o_home_val + o_away_val
            if o_total_similarity > total_similarity:
                total_similarity = o_total_similarity

            if total_similarity > max_similarity and total_similarity >= 1:
                self.logger.debug(f"Found newer, more likely game. {home} v {away} is {total_similarity} "
                                  f"away from the goal of {goal_home} vs {goal_away}")

                max_similarity = total_similarity
                guess = match

            print(f"{match}, {home} v {away} ({total_similarity})")


        return guess

