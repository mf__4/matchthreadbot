from dataclasses import dataclass, astuple, asdict, field


@dataclass(frozen=False, order=True)
class GameRequest:
    user_id: int
    user_name: str
    match_id: str = ""
    handled: bool = False
    home_team: str = ""
    away_team: str = ""
    subreddit: str = "SFMatchThreads"
    match_info: tuple = field(default_factory=tuple, compare=False)
    team1fix: str = ""
    team2fix: str = ""
    venue: str = ""
    ko_day: str = ""
    ko_time: str = ""
    status: str = ""
    comp: str = ""

    def __hash__(self):
        return hash(self.user_id) + hash(self.match_id) + hash(self.home_team) + hash(self.away_team)\
               + hash(self.ko_time) + hash(self.ko_day)

    def get_teams(self):
        return self.home_team, self.away_team

    def thread_creation_info(self):
        # avoid characters that the active_threads file uses
        return self.team1fix, self.team2fix, \
               f"mf__4", self.subreddit, self.match_id

    def add_match_info(self, match_info):
        self.match_info = match_info
        team1fix, _, team2fix, *_, venue, ko_day, ko_time, status, comp, _, _ = match_info
        self.team1fix = team1fix
        self.team2fix = team2fix
        self.venue = venue
        self.ko_day = ko_day
        self.ko_time = ko_time
        self.status = status
        self.comp = comp
