import os
import sys
import pathlib
import logging
from time import sleep
import telegram
import re

from telegram.error import BadRequest, NetworkError

top_dir = pathlib.Path(__file__).absolute().parent.parent.as_posix()
sys.path.append(top_dir)

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler
from telegram import InlineKeyboardMarkup, InlineKeyboardButton

from src.lib.logger.telegramHandler import TelegramLogHandler
from src.scraper import Scraper
from src.reddit_bot import RedditBot
from src.codes import *
from src.game_request import GameRequest

top_dir = pathlib.Path(__file__).absolute().parent.parent.as_posix()
sys.path.append(top_dir)
# Enable logging
LOG_DIR = os.path.join(os.getcwd(), 'logs')
LOG_FILENAME = os.path.join(LOG_DIR, 'rotating_log')
os.makedirs(LOG_DIR, exist_ok=True)
logging.addLevelName(LOGGING_CODES.MESG.value, 'MESG')
logging.addLevelName(LOGGING_CODES.RECMESG.value, 'RECMESG')
LOGGING_LEVELS = [logging.DEBUG, logging.INFO, logging.WARNING, logging.ERROR, logging.CRITICAL,
                  LOGGING_CODES.MESG.value, LOGGING_CODES.RECMESG.value]
LOG_FORMATTER = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

# Request callback stuff
RETURN_BUTTON = InlineKeyboardButton(text="Return", callback_data="return")
RETURN_KEYBOARD = InlineKeyboardMarkup([[RETURN_BUTTON]])
CLEAR_BUTTON = InlineKeyboardButton(text="Clear", callback_data="clear")
YES_BUTTON = InlineKeyboardButton(text="Yes", callback_data="yes")
NO_BUTTON = InlineKeyboardButton(text="No", callback_data="no")
request_buttons = [
    [
        InlineKeyboardButton("Home", callback_data="home_team"),
        InlineKeyboardButton("Away", callback_data="away_team")
    ],
    [
        InlineKeyboardButton("Subreddit", callback_data="sub"),
        InlineKeyboardButton("ESPN ID", callback_data="matchid")
    ],
    [
        InlineKeyboardButton("Cancel", callback_data="cancel"),
        InlineKeyboardButton("Search", callback_data="search")
    ]
]

max_log_bytes = 4000000
log_backup_count = 2

TIME_DELAY = 45  # seconds inbetween checking messages, updating threads etc

def log_message(func):
    def handle(self, update, context):
        """ Log any message received """
        message = update.effective_message
        user = update.effective_user
        chat = update.effective_chat

        if chat.type == "private":
            chat_name = f"Private Chat with {user.name}"
        else:
            chat_name = chat.title

        self.logger.log(
            level=LOGGING_CODES.RECMESG.value,
            msg=f"User {user.name}({str(user.id)}) has just sent a message in chat {chat_name} "
                f"({str(chat.id)}) saying \"{message.text}\"."
        )

        func(self, update, context)
    return handle

def handle_exceptions(func):
    def handle(self, *args, **kwargs):
        try:
            return func(self, *args, **kwargs)
        except NetworkError as exp:
            self.logger.error(f"Network error in {func.__name__}. {type(exp)} - {exp.args} - {exp}.")
            return ExceptionCodes.RETRY
        except Exception as exp:
            self.logger.critical(f"Unknown error in {func.__name__}. {type(exp)} - {exp.args} - {exp}.")
            return ExceptionCodes.RETRY

    return handle

class TelegramBot:
    def __init__(self, bot_token, owners, live=True):
        super().__init__()
        # Setup telegram bot
        self.updater = Updater(bot_token, use_context=True)
        self.bot = self.updater.bot
        self.owners = owners

        if isinstance(owners, int):
            self.owners = (owners,)
        elif isinstance(owners, list):
            assert len(owners) > 0
            self.owners = tuple(owners)
        elif isinstance(owners, tuple):
            assert len(owners) > 0
            self.owners = owners
        else:
            raise TypeError("Owners should be of type: int, tuple, list")
        assert all(isinstance(x, int) for x in self.owners)

        # Setup logging
        self.logger = logging.getLogger(__name__)
        for logger_level in LOGGING_LEVELS:
            name = logging.getLevelName(logger_level)
            handler = logging.handlers.RotatingFileHandler(
                LOG_FILENAME + "." + name + ".LOG",
                maxBytes=max_log_bytes,
                backupCount=log_backup_count
            )
            handler.setFormatter(LOG_FORMATTER)
            handler.setLevel(logger_level)
            self.logger.addHandler(handler)
        # Add stdout (print) logger
        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setFormatter(LOG_FORMATTER)
        stdout_handler.setLevel(logging.DEBUG)
        self.logger.addHandler(stdout_handler)

        self.bot_files_dir = os.path.join(os.getcwd(), 'bot_files')
        if not os.path.exists(self.bot_files_dir):
            os.mkdir(self.bot_files_dir)

        self.logger.setLevel(logging.DEBUG)

        # Add telegram logger
        for id in self.owners:
            telegram_logger = TelegramLogHandler(id, self.bot)
            telegram_logger.setLevel(logging.ERROR)
            telegram_logger.setFormatter(LOG_FORMATTER)
            self.logger.addHandler(telegram_logger)

        self.reply_handler = None
        self.target = None
        self.request_message_id = None
        self.request_buttons = request_buttons
        #todo add match thread references
        self.live = live  # will it actually post to reddit?
        self.messages_to_delete = []
        self.game_requests = {}


        default_sub = sys.argv[4]
        custom_subreddits = {}
        custom_subs = re.findall("(.[^,:]+),(.[^:,]+):?", sys.argv[9])
        if len(custom_subs) > 0:
            for team, sub in custom_subs:
                custom_subreddits[team] = sub
            self.logger.info(f"Custom subs in use are {custom_subreddits}")
        self.scraper = Scraper(self.logger, default_sub)
        if self.live:
            self.reddit_bot = RedditBot(sys.argv[1:9], self.logger, self.scraper, self, custom_subreddits)

    @handle_exceptions
    def run(self):

        self.send_owner_message(text="Starting")

        self.dp = self.updater.dispatcher

        self.dp.add_handler(CommandHandler("start", self.start))
        self.dp.add_handler(CommandHandler("active", self.active_threads))
        self.dp.add_handler(CommandHandler("request", self.request_game))
        self.dp.add_handler(CallbackQueryHandler(self.handle_callback))
        self.updater.start_polling()

        while self.live:
            self.reddit_bot.update_bot()
            self.logger.info(f"Sleeping for {TIME_DELAY} seconds...")
            sleep(TIME_DELAY)

        self.updater.idle()

    @log_message
    def text_log(self, update, context):
        """ Logs any non-command sent to the bot """
        pass

    def send_text(self, user_id, text):
        """ Generic method for sending text to a user. """
        logging_text = text.replace('\n\n', '\n\t\t')
        self.logger.log(
            level=LOGGING_CODES.MESG.value,
            msg=f"Bot sending message to ID {str(user_id)} saying {logging_text}."
        )

        self.bot.send_message(user_id, text)

    def send_owner_message(self, text):
        """ Generic method for sending text to a user. """
        text_no_nl = text.replace('\n\n', '\n\t\t')
        self.logger.log(
            level=LOGGING_CODES.MESG.value,
            msg=f"Bot sending message to owners saying {text_no_nl}."
        )

        for owner_id in self.owners:
            self.bot.send_message(owner_id, text)

    @log_message
    def start(self, update, context):
        message_to_send = f"Hello, welcome to SFMatchThreader bot. This telegram bot is used to interact with " \
                          f"u/SFMatchThreadder on reddit - which posts match threads on r/ScottishFootball."

        self.send_text(update.effective_user.id, message_to_send)

    @log_message
    def active_threads(self, update, context):
        activeThreads = []
        self.logger.info("Reading in active threads from file")
        f = open(os.path.join(self.bot_files_dir, 'active_threads.txt'), 'r')
        s = f.read()
        self.logger.info(f"Data read in from active threads file is: {s}")

        info = s.split('&&&&')
        num_threads = 0
        if info[0] != '':
            for d in info:
                [match_id, t1, t2, thread_id, reqr, sub, thread_type] = d.split('####')
                data = match_id, t1, t2, thread_id, reqr, sub, thread_type
                activeThreads.append(data)
                num_threads = len(activeThreads)
                self.logger.info(f"Active threads: {num_threads} - added {t1} vs {t2} (/r/{sub})")
        f.close()

        if len(activeThreads) == 0:
            message = "There is no active threads."
        else:
            message = f"There is {num_threads} active thread"
            if num_threads > 1:
                message += "s"

            message += ": \n\n"
            for t in activeThreads:
                match_id, t1, t2, thread_id, reqr, sub, thread_type = t
                message += f"{t1} vs {t2} in r/{sub}. \n" \
                           f"ESPN: https://www.espn.com/soccer/match?gameId={match_id}.\n" \
                           f"Reddit: https://reddit.com/{thread_id}.\n\n"

        self.send_text(update.effective_user.id, message)

    @log_message
    def request_game(self, update, context):
        """Generate options keyboard"""
        user_id = update.effective_user.id
        user_name = update.effective_user.name
        print(type(user_id))
        game_request = GameRequest(user_id=user_id, user_name=user_name)
        self.game_requests[user_id] = game_request
        markup = InlineKeyboardMarkup(self.request_buttons)
        self.request_message_id = update.message.reply_text(
            reply_markup=InlineKeyboardMarkup(self.request_buttons),
            text=self.build_message(game_request),
            parse_mode=telegram.ParseMode.HTML
        )["message_id"]

    # command helpers

    def handle_callback(self, update, context):
        """Handles callback request"""
        options = ["home_team", "away_team", "sub", "search", "yes", "no"]
        query = update.callback_query
        user_id = update.effective_user.id
        reply = query.data
        print(f"Got callback {reply}")

        if reply in options:
            self.target = reply
            self.get_reply(update, query)
        elif reply == "return":
            self.reply_entered(update, context)
        elif self.target == "cancel":
            self.bot.deleteMessage(
                chat_id=query.message.chat.id,
                message_id=query.message.message_id
            )
        else:
            self.logger.info("Unexpected callback query: {}".format(self.target))
        query.answer()

    def get_reply(self, update, query):
        user_id = update.effective_user.id
        game_request = self.game_requests[user_id]
        parse_mode = None
        if self.target == 'home_team':
            message = "Who is playing at home?"
            markup = RETURN_KEYBOARD
        elif self.target == 'away_team':
            message = "Who is playing away?"
            markup = RETURN_KEYBOARD
        elif self.target == 'sub':
            message = "What subreddit is this for?"
            markup = RETURN_KEYBOARD
        elif self.target == 'search':
            message = "Searching..."
            markup = None
            self.bot.edit_message_text(
                chat_id=update.effective_chat.id,
                message_id=self.request_message_id,
                reply_markup=markup,
                text=message
            )

            if game_request.home_team == "" and game_request.away_team == "":
                message = "Provide at least a home or away team"
                self.target = "home_team"
                markup = RETURN_KEYBOARD
            else:
                match_id = self.search(update)
                if match_id != ExceptionCodes.NO_MATCH:
                    team1fix, _, team2fix, *_, venue, ko_day, ko_time, status, comp, _, _ = game_request.match_info
                    message = f"I think that game is {team1fix} vs {team2fix} ({status}) in the {comp} at {ko_time} " \
                              f"(http://www.espn.com/soccer/match?gameId={match_id}). Is this correct?"
                    markup = InlineKeyboardMarkup([[YES_BUTTON, NO_BUTTON], [CLEAR_BUTTON]])
                else:
                    message = "Sorry I can't find that game, double check the information."

            # self.bot.edit_message_text(
            #     chat_id=update.effective_chat.id,
            #     message_id=self.request_message_id,
            #     reply_markup=markup,
            #     text=message,
            #     parse_mode=telegram.ParseMode.HTML
            # )

        elif self.target == 'yes':
            print("recieved a yes")

            return self.submit(update, query)
        else:
            return
        query.edit_message_text(
            text=message,
            reply_markup=markup,
            parse_mode=parse_mode
        )
        self.reply_handler = MessageHandler(Filters.user(user_id), self.reply_entered)
        self.dp.add_handler(self.reply_handler)

    def reply_entered(self, update, context):

        game_request = self.game_requests[update.effective_user.id]
        reply_markup = None
        if update.message is not None:
            self.messages_to_delete.append(update.message.message_id)
        for message in self.messages_to_delete:
            self.bot.delete_message(
                chat_id=update.effective_chat.id,
                message_id=message
            )

        self.messages_to_delete = []
        if update.message is not None:
            reply = update.message.text
        else:
            reply = update.callback_query.data
        reply = reply.strip()
        valid = False
        if self.target == 'home_team':
            if reply.lower() != "return":
                game_request.home_team = reply

            valid = True
            self.target = None

        if self.target == 'away_team':
            if reply.lower() != "return":
                game_request.away_team = reply

            valid = True
            self.target = None
        if self.target == 'sub':
            if reply.lower() != "return":
                game_request.subreddit = reply

            valid = True
            self.target = None


        if self.target == 'yes':
            if reply.lower() != "return":
                game_request.subreddit = reply

            valid = True
            self.target = None
            print("YES")

        if self.target == 'no':
            valid = True
            self.target = None
            self.game_requests[update.effective_user.id].match_id = ""

        if valid:
            self.target = None
            self.dp.remove_handler(self.reply_handler)

            self.bot.edit_message_text(
                chat_id=update.effective_chat.id,
                message_id=self.request_message_id,
                reply_markup=InlineKeyboardMarkup(self.request_buttons),
                text=self.build_message(game_request),
                parse_mode=telegram.ParseMode.HTML
            )
        else:
            if not reply_markup:
                reply_markup = InlineKeyboardMarkup([[RETURN_BUTTON], [CLEAR_BUTTON]])
            try:
                self.bot.edit_message_text(
                    chat_id=update.effective_chat.id,
                    message_id=self.request_message_id,
                    text="Sorry that was invalid, please try again",
                    reply_markup=reply_markup
                )
            except BadRequest as e:
                if e.message == "Message is not modified: specified new message content and" \
                                " reply markup are exactly the same as a current " \
                                "content and reply markup of the message":
                    pass
                else:
                    raise e
            except Exception as exp:
                print(f"Oh no. {type(exp)} - {exp}")

    def build_message(self, game_request):
        print("Building Message")
        home_team = game_request.home_team if game_request.home_team is not None else "None"
        away_team = game_request.away_team if game_request.away_team is not None else "None"
        subreddit = game_request.subreddit if game_request.subreddit is not None else "None"


        message = f"<b>Game</b>\n\n" \
                  f"<b>{home_team}</b> vs <b>{away_team}</b>\n" \
                  f"<b>Subreddit:</b> {subreddit}"


        return message

    def search(self, update):
        user_id = update.effective_user.id
        game_request = self.game_requests[user_id]
        home, away = game_request.get_teams()
        if home == "":
            match_id = self.scraper.findMatchSiteExact(game_request.away_team)
        elif away == "":
            match_id = self.scraper.findMatchSiteExact(game_request.home_team)
        else:
            match_id = self.scraper.findMatchSite(game_request.home_team, game_request.away_team)
        self.logger.info(f"Found id {match_id} for {home} vs {away}")
        if match_id != ExceptionCodes.NO_MATCH:
            game_request.match_id = match_id
            match_info = self.scraper.getMatchInfo(match_id)
            game_request.add_match_info(match_info)

        return match_id

    def submit(self, update, query):
        user_id = update.effective_user.id
        game_request = self.game_requests[user_id]
        team1, team2, reqr, sub, direct = game_request.thread_creation_info()
        message = f"Submitting thread for {team1} vs {team2} on /r/{sub}..."

        print(f"Submitting thread for {team1} vs {team2} on /r/{sub}")
        markup = RETURN_KEYBOARD
        self.bot.edit_message_text(
            chat_id=update.effective_chat.id,
            message_id=self.request_message_id,
            reply_markup=markup,
            text=message
        )
        code, thread_id = self.reddit_bot.createNewThread(team1, team2, reqr, sub, direct)
        game_request.handled = True
        if code == ThreadCodes.SUCCESS:
            message = f"Thread successfull posted. Link: reddit.com/r/{sub}/comments/{thread_id}"
            markup = RETURN_KEYBOARD
        elif code == ThreadCodes.TOO_EARLY:
            message = f"Please wait until atleast 5 minutes before kickoff to request a thread."
            markup = RETURN_KEYBOARD
        elif code == ThreadCodes.THREAD_EXISTS:
            message = f"There is already a [match thread](http://www.reddit.com/r/{sub}/" \
                      f"comments/{thread_id}) for that game. Join the discussion there!"
            markup = RETURN_KEYBOARD
        elif code == ThreadCodes.BAD_SUBREDDIT:
            message = f"Sorry I can't post to /r/{sub}. I only post to Scottish related subs. If /r/{sub} is " \
                      f"a Scottish sub, message my personal account on reddit (/u/mf__4) and I'll fix it."
            markup = RETURN_KEYBOARD
        elif code == ThreadCodes.GAME_OVER:
            message = f"This game appears to be finished. If it is not please try again and message my personal " \
                      f"account if this keeps happening.."
            markup = RETURN_KEYBOARD
        elif code == ThreadCodes.FAILURE:
            message = f"Sorry I can't wasn't able to post that. Please try again and message my personal account" \
                      f" if this keeps happening.."
            markup = RETURN_KEYBOARD
        del self.game_requests[user_id]
        query.answer(text=message)
        self.bot.send_message(user_id, message)
        self.bot.deleteMessage(
            chat_id=query.message.chat.id,
            message_id=query.message.message_id
        )

*_, token, owner = sys.argv
try:
    owner = int(owner)
except TypeError:
    print(f"Something bad happened. Owner id provided ({owner}) is not an int")
if __name__ == '__main__':
    bot = TelegramBot(token, [owner], live=True)
    while True:
        status = bot.run()
        if status == ExceptionCodes.FAILURE:
            bot.send_owner_message(text="Network Error. Restarting...")
        elif status == ExceptionCodes.RETRY:
            bot.send_owner_message(text="Unknown Error. Restarting...")


