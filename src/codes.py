from enum import Enum, auto


class ThreadCodes(Enum):
    SUCCESS = auto()
    FAILURE = auto()  # use better names, placeholder rn
    NO_MATCH = auto()

    TOO_EARLY = auto()
    GAME_OVER = auto()
    THREAD_EXISTS = auto()

    BAD_SUBREDDIT = auto()
    BAD_USER = auto()
    BLACKLIST = auto()  # is this different than bad user?


class ExceptionCodes(Enum):
    SUCCESS = auto()
    NO_MATCH = auto()
    RETRY = auto()
    CONNECTION_FAILED = auto()

    FAILURE = auto()

    UNKNOWN_FAILURE = auto()


class LOGGING_CODES(Enum):
    MESG = 31
    RECMESG = 35
