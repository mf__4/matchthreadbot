import praw
import re
import datetime
import requests
import requests.auth
import prawcore.exceptions as praw_exception
from time import sleep
from src.codes import *

# markup constants
goal = 0
pgoal = 1
ogoal = 2
mpen = 3
yel = 4
syel = 5
red = 6
subst = 7
subo = 8
subi = 9
strms = 10
lines = 11
evnts = 12



messaging = True
# naughty list
usrblacklist = ['dbawbaby',
                '12F12',
                'KYAmiibro']

# allowed to make multiple threads
usrwhitelist = ['mf__4', 'Hardman_Al']  # me, irl mate

# adjust time limit in given subreddit
custTimeLimit = {'sfmatchthreads': [30]}

# no time limit to when user can post in specific subreddit
timewhitelist = {'sfmatchthreads': ['mf__4'],
                 'scottishfootball': ['mf__4', 'Hardman_Al']}


def requires_whitelist(func):
    def handle(self, **kwargs):
        print("Implement whitelist decorator")
        return func(self, kwargs)

    return handle


def requires_admin(func):
    def handle(self, **kwargs):
        print("Implement admin decorator")
        return func(self, kwargs)

    return handle


def requires_owner(func):
    def handle(self, **kwargs):
        print("Implement owner decorator")
        return func(self, kwargs)

    return handle



class RedditBot:
    def __init__(self, login_details, logger, scraper, telegram_bot, custom_subs):
        self.logger = logger
        print("reddit bot init")
        login = self.setup(login_details)
        self.reddit, self.admin, self.bot_username, _, self.default_subreddit, *_ = login
        self.activeThreads = []
        self.read_active_threads()
        self.post_match_attempts = {}

        self.match_info = {}

        self.scottishfootball_attempts = 31
        self.scottishfootball_tracked_teams = {}

        self.customsubs_attempts = 31
        self.custom_subs_tracked_teams = {}
        self.customsubs = custom_subs

        self.bbcgames_attempts = 31
        self.bbcgames_tracked_teams = {}

        # todo give these values
        self.scraper = scraper
        self.telegram_bot = telegram_bot

    def setup(self, login_details):
        try:

            self.logger.debug(f"Reddit login details are {login_details}")
            admin, username, password, subreddit, user_agent, client_id, secret, redirect = login_details
            r = praw.Reddit(client_id=client_id, client_secret=secret, username=username, password=password,
                            user_agent=user_agent)

            usrwhitelist.append(username)

            moderators = ""
            for moderator in r.subreddit(subreddit).moderator():
                moderators += f"{moderator}, "
                if moderator.name not in usrwhitelist:
                    usrwhitelist.append(moderator.name)
            moderators += "\b\b"

            self.logger.debug(f"Listing r/{subreddit}'s moderators: {moderators}")

            self.logger.info(f"Allowed to post multiple threads: {usrwhitelist}")

            return r, admin, username, password, subreddit, user_agent, client_id, secret, redirect
        except Exception as excep:
            self.logger.critical("Setup error: please ensure environment variables exists in its correct form "
                                 f"(check readme for more info) - {excep}")
            sleep(10)

    # save activeThreads
    def save_active_threads(self):
        with open('bot_files/active_threads.txt', 'w+') as thread_file:
            thread_data = ''
            self.logger.info("Saving active threads data")
            for data in self.activeThreads:
                match_id, t1, t2, thread_id, reqr, sub, thread_type = data
                if type(reqr) is not str:
                    reqr = reqr.name
                thread_data += f'{match_id}####{t1}####{t2}####{thread_id}####{reqr}####{sub}####{thread_type}&&&&'
            thread_data = thread_data[0:-4]  # take off last &&&&
            thread_file.write(thread_data)

    # read saved activeThreads data
    def read_active_threads(self):
        self.logger.info("Reading in active threads from file")

        with open('bot_files/active_threads.txt', 'r') as thread_file:
            s = thread_file.read()
            self.logger.info(f"Active threads read in from file is: {s}")
            info = s.split('&&&&')
            if info[0] != '':
                for d in info:
                    [match_id, t1, t2, thread_id, reqr, sub, thread_type] = d.split('####')
                    data = match_id, t1, t2, thread_id, reqr, sub, thread_type
                    self.activeThreads.append(data)
                    self.logger.info(f"Active threads: {len(self.activeThreads)} - added {t1} vs {t2} (/r/{sub})")

    @requires_admin
    def resetAll(self):
        removeList = list(self.activeThreads)
        for data in removeList:
            self.activeThreads.remove(data)
            _, t1, t2, _, _, sub, thread_type = data
            self.logger.info(f"Active threads: {len(self.activeThreads)} - removed {t1} vs {t2} (/r/{sub})")
            self.save_active_threads()


    def loadMarkup(self, subreddit):
        try:
            markup = [line.rstrip('\n') for line in open(subreddit + '.txt')]
        except:
            markup = [line.rstrip('\n') for line in open('soccer.txt')]
        return markup

    def writeLineUps(self, sub, body, t1, t1id, t2, t2id, team1Start, team1Sub, team2Start, team2Sub):
        markup = self.loadMarkup(sub)
        t1sprite = ''
        t2sprite = ''

        body += '**LINE-UPS**\n\n**' + t1sprite + t1 + '**\n\n'
        linestring = ''
        # write team 1 lineup out
        for i, name in enumerate(team1Start):
            # was this player a sub?
            if '!sub' in name:
                linestring += markup[subst] + name[5:]
            else:
                # is the next player a sub? strikethrough name if they are as means theyve been subbed off
                if i + 1 < len(team1Start):
                    if '!sub' in team1Start[i + 1]:
                        linestring += ', ~~' + name + '~~ '
                    else:
                        linestring += ', ' + name
                else:
                    linestring += ', ' + name


        linestring = linestring[2:] + '.\n\n'
        body += linestring + '**Subs:** '
        body += ", ".join(x for x in team1Sub) + ".\n\n^____________________________\n\n"

        body += '**' + t2sprite + t2 + '**\n\n'
        linestring = ''
        # write team 2 lineup out
        for i, name in enumerate(team2Start):
            # was this player subbed on?
            if '!sub' in name:
                linestring += markup[subst] + name[5:]
            else:
                # is the next player a sub? strikethrough name if they are as means theyve been subbed off
                if i + 1 < len(team2Start):
                    if '!sub' in team2Start[i + 1]:
                        linestring += ', ~~' + name + '~~ '
                    else:
                        linestring += ', ' + name
                else:
                    linestring += ', ' + name

        linestring = linestring[2:] + '.\n\n'
        body += linestring + '**Subs:** '
        body += ", ".join(x for x in team2Sub) + "."

        return body


    # attempt submission to subreddit
    def submitThread(self, sub, title, reqr):
        self.logger.info(f"Submitting {title}...")
        try:
            thread = self.reddit.subreddit(sub).submit(title, selftext='**Venue:**\n\n**LINE-UPS**', send_replies=False)
            # thread = self.reddit.subreddit("SFMatchThreads").submit(title,selftext='**Venue:**\n\n**LINE-UPS**',send_replies=False)
            self.logger.info(f"Thread ID: {thread} submitted for subreddit r/{sub}.")
            self.telegram_bot.send_owner_message(f"New thread created. Link: reddit.com/r/{sub}/comments/{thread}. "
                                                 f"Requested by {reqr}")
            return True, thread
        except Exception as exception:
            self.logger.critical(f"SUBMIT THREAD FAILED. {exception}")
            return False, ''

    # todo break this into multiple functions
    def createNewThread(self, team1, team2, reqr, sub, direct):
        if direct == '':
            matchID = self.scraper.findMatchSite(team1, team2)
        else:
            matchID = direct
        if matchID != ExceptionCodes.NO_MATCH:
            gotinfo = False
            while not gotinfo:
                try:
                    t1, t1id, t2, t2id, team1Start, team1Sub, team2Start, team2Sub, \
                    venue, ko_day, ko_time, status, comp, t1abb, t2abb = self.scraper.getMatchInfo(matchID)
                    gotinfo = True
                except requests.exceptions.Timeout:
                    self.logger.warning(f"ESPNFC access timeout for {team1} vs {team2}.")

            # only post to related subreddits
            relatedsubs = self.scraper.get_related_subs()
            if sub.lower() not in relatedsubs:
                self.logger.warning(f"Denied post request to /r/{sub} - not related")
                return ThreadCodes.BAD_SUBREDDIT, ''

            # don't post if user is blacklisted
            if reqr in usrblacklist:
                self.logger.warning(f"Denied post request from /u/{reqr} - blacklisted")
                return ThreadCodes.BLACKLIST, ''

            # don't create a thread if the bot already made it or if user already has an active thread
            for d in self.activeThreads:
                matchID_at, t1_at, t2_at, id_at, reqr_at, sub_at, thread_type = d
                if t1 == t1_at and sub == sub_at:
                    self.logger.info(f"Denied {t1} vs {t2} request for /r/{sub} - thread already exists")
                    return ThreadCodes.THREAD_EXISTS, id_at
                if reqr == reqr_at and reqr not in usrwhitelist:
                    self.logger.warning(f"Denied post request from /u/{reqr} - has an active thread request")

                    message = f"Thread request from /u/{reqr} has been rejected as they already have an active " \
                              f"thread and are not in the whitelist, is this correct? whitelist is {usrwhitelist}"

                    self.telegram_bot.send_owner_message(message)
                    return ThreadCodes.BAD_USER, ''

            # don't create a thread if the match is done (probably found the wrong match) or postponed
            if status.startswith('FT') or status == 'AET' or status == 'Postponed':
                self.logger.info(f"Denied {t1} vs {t2} request - match appears to be finished")
                return ThreadCodes.GAME_OVER, ''

            timelimit = 15

            if sub.lower() in custTimeLimit:
                timelimit = custTimeLimit[sub.lower()][0]
            # don't create a thread more than 5 minutes before kickoff
            if type(reqr) is not str:
                reqr = reqr.name
            if sub.lower() not in timewhitelist or sub.lower() in timewhitelist and reqr.lower() not in timewhitelist[sub.lower()]:
                hour_i, min_i = self.scraper.getTimes(ko_time)
                now = datetime.datetime.now()
                now_f = now + datetime.timedelta(hours=0, minutes=timelimit)  # timezone
                self.logger.debug(f"Allowed Time: {now_f}; Game Time: {ko_time}")

                if ko_day == '':
                    return ThreadCodes.FAILURE, ''
                if now_f.day < int(ko_day):
                    self.logger.info(f"Denied {t1} vs {t2} request - more than {timelimit} minutes to kickoff")
                    return ThreadCodes.TOO_EARLY, ''
                if now_f.hour < hour_i:
                    self.logger.info(f"Denied {t1} vs {t2} request - more than {timelimit} minutes to kickoff")
                    return ThreadCodes.TOO_EARLY, ''
                if (now_f.hour == hour_i) and (now_f.minute < min_i):
                    self.logger.info(f"Denied {t1} vs {t2} request - more than {timelimit} minutes to kickoff")
                    return ThreadCodes.TOO_EARLY, ''

            # competition logic, go here to remove friendly games when i know what it looks like
            title = 'Match Thread: ' + t1 + ' vs ' + t2
            if comp != '':
                title = title + ' | ' + comp

            # find if on BBC
            tv_channel = self.scraper.grabTV(t1, t2)
            if isinstance(tv_channel, str):
                if "BBC" in tv_channel:
                    title += " [BBC]"

            result, thread = self.submitThread(sub, title, reqr)

            # if subreddit was invalid, notify
            if not result:
                return ThreadCodes.FAILURE, ''

            # try and set the flair to "Match Thread" if possible
            try:
                flairs = thread.flair.choices()
                self.logger.debug(f"Available flairs are {flairs}")

                flair_id = -1
                for flair in flairs:
                    if 'Match Thread'.lower() == flair['flair_text'].lower():
                        flair_id = flair['flair_template_id']

                if flair_id != -1:
                    thread.flair.select(flair_id)
            except praw_exception.Forbidden:
                self.logger.info(f"Flairs not allowed on this subreddit (r/{sub})")

            short = thread.shortlink
            thread_id = short[short.index('.it/') + 4:].encode("utf8")
            t_id = re.findall("b'(.*)'", str(thread_id))[0]
            redditstream = 'http://www.reddit-stream.com/comments/' + str(t_id)

            data = matchID, t1, t2, t_id, reqr, sub, "match"

            self.activeThreads.append(data)
            self.save_active_threads()
            self.logger.info(f"Active threads: {len(self.activeThreads)} - added {t1} vs {t2} (/r/{sub})")

            if status == 'v':
                status = "0'"

            markup = self.loadMarkup(sub)

            body = '#**' + status + ": " + t1 + ' vs ' + t2 + '**\n\n'

            body += '**Venue:** ' + venue + '\n\n'
            tv = self.scraper.grabTV(t1, t2)
            if type(tv) is not ExceptionCodes:
                body += tv
            body += '[Auto-refreshing reddit comments link](' + redditstream + ')\n\n---------\n\n'

            body += markup[lines] + ' '
            body = self.writeLineUps(sub, body, t1, t1id, t2, t2id, team1Start, team1Sub, team2Start, team2Sub)

            # add stats
            code, stats = self.scraper.grabStats(t1, t2)
            if code == ExceptionCodes.SUCCESS:
                body += stats

            # "[^[Request ^a ^match ^thread]](http://www.reddit.com/message/compose/?to=SFMatchThreadder&subject=Match%20Thread&message=Team%20vs%20Team)"
            body += f'\n\n------------\n\n{markup[evnts]}**MATCH EVENTS** | *via ' \
                    f'[ESPN](http://www.espn.com/soccer/match?gameId={matchID})*\n\n'

            body += f"\n\n--------\n\n*^(Don't see a thread for a match you're watching?) " \
                    f"[^(Click here)](https://www.reddit.com/user/SFMatchThreadder/comments/l48zcl/) " \
                    f"^(to learn how to request a match thread from this bot.)*"

            thread.edit(body)
            sleep(5)

            return ThreadCodes.SUCCESS, t_id
        else:
            self.logger.warning(f"Could not find match info for {team1} vs {team2}")
            return ThreadCodes.NO_MATCH, ''

    # if the requester just wants a template
    def createMatchInfo(self, team1, team2):
        matchID = self.scraper.findMatchSite(team1, team2)
        if matchID != 'no match':
            t1, t1id, t2, t2id, team1Start, team1Sub, team2Start, team2Sub, venue, ko_day, ko_time, status, comp, t1abb, t2abb = getMatchInfo(
                matchID)

            markup = self.loadMarkup('soccer')

            body = f'#**{status}: {t1} vs {t2}**\n\n'
            body += f'**Venue:** {venue}\n\n--------\n\n'
            body += markup[lines] + ' '
            body = self.writeLineUps('soccer', body, t1, t1id, t2, t2id, team1Start, team1Sub, team2Start, team2Sub)

            body += f'\n\n------------\n\n{markup[evnts]}**MATCH EVENTS**\n\n'

            self.logger.info(f"Created template for {t1} vs {t2}.")
            return ThreadCodes.SUCCESS, body
        else:
            return ThreadCodes.NO_MATCH, ''

    # delete a thread (on admin request)
    @requires_admin
    def delete_thread(self, thread_id):
        try:
            if '//' in thread_id:
                thread_id = re.findall('comments/(.*?)/', thread_id)[0]
            thread = self.reddit.submission(id=thread_id)
            for data in self.activeThreads:
                _, team1, team2, *_, sub, thread_type = data
                if thread_id == thread_id:
                    thread.delete()
                    self.activeThreads.remove(data)
                    self.logger.info(f"Active threads: {len(self.activeThreads)} "
                                     f"- removed {team1} vs {team2} (/r/{sub})")
                    self.save_active_threads()
                    return team1 + ' vs ' + team2
            return ''
        except:
            return ''

    # remove incorrectly made thread if requester asks within 5 minutes of creation
    def remove_wrong_thread(self, thread_id, req):
        try:
            thread = self.reddit.submission(id=thread_id)
            dif = datetime.datetime.utcnow() - datetime.datetime.utcfromtimestamp(thread.created_utc)
            for data in self.activeThreads:
                match_id, team1, team2, thread_id, reqr, sub, thread_type = data
                if thread_id == thread_id:
                    if reqr != req:
                        return 'req'
                    if dif.days != 0 or dif.seconds > 300:
                        return 'time'
                    thread.delete()
                    self.activeThreads.remove(data)
                    self.logger.info(f"Active threads: {len(self.activeThreads)} "
                                     f"- removed {team1} vs {team2} (/r/{sub})")
                    self.save_active_threads()
                    return team1 + ' vs ' + team2
            return 'thread'
        except:
            return 'thread'

    # default attempt to find teams: split input in half, left vs right
    def firstTryTeams(self, msg):
        t = msg.split()
        spl = int(len(t) / 2)
        t1 = t[0:spl]
        t2 = t[spl + 1:]
        t1s = ''
        t2s = ''
        for word in t1:
            t1s += word + ' '
        for word in t2:
            t2s += word + ' '
        return [t1s, t2s]

    # check for new mail, create new threads if needed
    # todo split this up a bit
    def check_and_create(self):
        self.logger.info("Checking inbox...")

        delims = [' x ', ' - ', ' v ', ' vs ']

        subdel = ' for '
        for msg in self.reddit.inbox.unread(mark_read=False):
            sub = self.default_subreddit
            message = f"New DM received from /u/{msg.author.name}.\n\n\n{msg.subject}:\n\n{msg.body}"
            self.telegram_bot.send_owner_message(message)
            msg.mark_read()
            if msg.subject.lower() == 'match thread':
                user_request = msg.body.split(subdel, 2)
                if user_request[0] != msg.body:
                    # user has provided a subreddit, not default, get subreddit
                    sub = user_request[1].split('/')[-1]
                    sub = sub.lower()
                    sub = sub.rstrip()

                teams = self.firstTryTeams(user_request[0])
                delim_found = False
                for delim in delims:
                    if delim in user_request[0]:
                        delim_found = True
                    attempt = user_request[0].split(delim, 2)
                    if attempt[0] != user_request[0]:
                        teams = attempt

                match_id = None
                if not delim_found:
                    try:
                        # is it a match id? will be able to parse to int if so
                        _ = int(user_request[0])
                        match_id = user_request[0]
                    except ValueError:
                        self.logger.error(f"Message {msg.body} appears to only have one team")
                if match_id is None:
                    thread_status, thread_id = self.createNewThread(teams[0], teams[1], msg.author, sub, '')
                else:
                    thread_status, thread_id = self.createNewThread('', '', msg.author, sub, match_id)
                if messaging:
                    if thread_status == ThreadCodes.SUCCESS:
                        # thread created successfully
                        msg.reply(f"[Here](http://www.reddit.com/r/{sub}/comments/{thread_id}) "
                                  f"is a link to the thread you've requested. Thanks for using this bot!"
                                  f"\n\n-------------------------\n\n*Did I create a thread for the wrong "
                                  f"match? [Click here and press send]"
                                  f"(http://www.reddit.com/message/compose/?to={self.bot_username}&subject="
                                  f"delete&message={thread_id}) to delete the thread (note: this will only work "
                                  f"within five minutes of the thread's creation). This probably means that I can't "
                                  f"find the right match - sorry!*")

                        message = f"Match thread request fulfilled /u/{msg.author.name} requested " \
                                  f"{teams[0]} vs {teams[1]} in /r/{sub}. " \
                                  f"\n\nThread: http://www.reddit.com/r/{sub}/comments/{thread_id}\n" \
                                  f"Delete: http://www.reddit.com/message/compose/?to={self.bot_username}" \
                                  f"&subject=delete&message={thread_id}"

                        self.telegram_bot.send_owner_message(message)
                    if thread_status == ThreadCodes.NO_MATCH:
                        # not found
                        msg.reply("Sorry, I couldn't find info for that match. "
                                  "In the future I'll account for more matches around the world."
                                  "\n\n-------------------------\n\n*Why not run your own match thread? "
                                  "[Look here](https://www.reddit.com/r/soccer/wiki/matchthreads) for templates, "
                                  "tips, and example match threads from the past if you're not sure how.*\n\n")
                        # todo mention telegram bot here when i make it

                    if thread_status == ThreadCodes.TOO_EARLY:
                        # before kickoff
                        msg.reply("Please wait until at least 5 minutes to kickoff to send me a thread request. "
                                  "Thanks!\n\n-------------------------\n\n*Why not run your own match thread? "
                                  "[Look here](https://www.reddit.com/r/soccer/wiki/matchthreads) for templates, "
                                  "tips, and example match threads from the past if you're not sure how.*\n\n*")

                    if thread_status == ThreadCodes.GAME_OVER:
                        # after full time - probably found the wrong match
                        msg.reply("Sorry, I couldn't find a currently live match with those teams - are you sure "
                                  "the match has started (and hasn't finished)? If you think this is a mistake, "
                                  "it probably means I can't find that match.\n\n-------------------------\n\n*"
                                  "Why not run your own match thread? [Look here](https://www.reddit.com/r/soccer/wiki/"
                                  "matchthreads) for templates, tips, and example match threads from the past if "
                                  "you're not sure how.*\n\n")

                    if thread_status == ThreadCodes.THREAD_EXISTS:
                        # thread already exists
                        msg.reply(f"There is already a [match thread](http://www.reddit.com/r/{sub}/"
                                  f"comments/{thread_id}) for that game. Join the discussion there!")

                    if thread_status == ThreadCodes.BAD_SUBREDDIT:
                        # invalid subreddit
                        msg.reply(f"Sorry, I can't post to /r/{sub}. If it is a valid sub I may not be allowed to post "
                                  f"there or I may have hit a posting limit.")

                    if thread_status == ThreadCodes.BAD_USER:
                        # thread limit
                        msg.reply(f"Sorry, you can only have one active thread request at a time. If you are a mod "
                                  f"and think you should be able to bypass this limit, message /u/{self.admin} and "
                                  f"I'll sort it")

                    if thread_status == ThreadCodes.BLACKLIST:
                        # thread limit
                        msg.reply(f"Sorry, you appear to be blacklisted from this bot. If you think this is"
                                  f"an issue message /u/{self.admin}.")

                    if thread_status == ThreadCodes.FAILURE:
                        # sub blacklisted
                        msg.reply(
                            f"Sorry, I wasn't able to make the thread. Please try again or message /u/{self.admin} "
                            f"to see what the issue was")

            if msg.subject.lower() == 'match info':
                teams = self.firstTryTeams(msg.body)
                for delim in delims:
                    attempt = msg.body.split(delim, 2)
                    if attempt[0] != msg.body:
                        teams = attempt
                thread_status, text = self.createMatchInfo(teams[0], teams[1])
                if thread_status == 0:  # successfully found info
                    msg.reply(
                        "Below is the information for the match you've requested.\n\nIf you're using [RES](http://redditenhancementsuite.com/), you can use the 'source' button below this message to copy/paste the exact formatting code. If you aren't, you'll have to add the formatting yourself.\n\n----------\n\n" + text)
                if thread_status == 1:  # not found
                    msg.reply(
                        "Sorry, I couldn't find info for that match. In the future I'll account for more matches around the world.")

            if msg.subject.lower() == 'delete':
                if msg.author == self.admin:
                    name = self.delete_thread(msg.body)
                    if messaging:
                        if name != '':
                            msg.reply("Deleted " + name)
                        else:
                            msg.reply("Thread not found")
                else:
                    name = self.remove_wrong_thread(msg.body, msg.author)
                    if messaging:
                        if name == 'thread':
                            msg.reply("Thread not found - please double-check thread ID")
                        elif name == 'time':
                            msg.reply(f"This thread is more than five minutes old - thread deletion from now is an "
                                      f"admin feature only. You can message /u/{self.admin} if you'd still like the "
                                      f"thread to be deleted.")
                        elif name == 'req':
                            msg.reply("Username not recognised. Only the thread requester and bot admin have "
                                      "access to this feature.")
                        else:
                            msg.reply("Deleted " + name)

        self.logger.info("All messages checked.")

    # update all current threads
    def update_threads(self):
        toRemove = []

        for data in self.activeThreads:
            finished = False
            index = self.activeThreads.index(data)
            match_id, team1, team2, thread_id, reqr, sub, thread_type = data
            thread = self.reddit.submission(id=thread_id)
            body = thread.selftext

            venue_index = body.index('**Venue:**')

            markup = self.loadMarkup(sub)
            # detect if finished
            match_status = self.scraper.getStatus(match_id)
            if match_status == 'FT' or match_status == 'AET':
                finished = True
            elif match_status == 'FT-Pens' or match_status == 'PEN':
                info = self.scraper.getExtraInfo(match_id)
                self.logger.debug(f"INFO for {team1} v {team2}: {info}")
                if 'won' in info or 'win' in info or 'advance' in info:
                    finished = True

            # update lineups
            team1_start, team1_sub, team2_start, team2_sub = self.scraper.getLineUps(match_id)
            lineup_index = body.index('**LINE-UPS**')
            body_til_then = body[venue_index:lineup_index]

            t1id, t2id = self.scraper.getTeamIDs(match_id)
            newbody = self.writeLineUps(sub, body_til_then, team1, t1id, team2, t2id,
                                        team1_start, team1_sub, team2_start, team2_sub)

            # add stats
            code, stats = self.scraper.grabStats(team1, team2)
            if code == ExceptionCodes.SUCCESS:
                newbody += stats

            newbody += f'\n\n------------\n\n{markup[evnts]}**MATCH EVENTS** | ' \
                       f'*via [ESPN](http://www.espn.com/soccer/match?gameId={match_id})*\n\n'

            # update scorelines
            score = self.scraper.updateScore(match_id, team1, team2, sub)
            newbody = f"{score}\n\n--------\n\n{newbody}"

            events = self.scraper.grabEvents(match_id, markup)

            # add link to goals
            events = self.getGoalLinks(thread, events)

            newbody += '\n\n' + events

            # remove strong tags that happen at own goals
            if "<strong>" in newbody or "</strong>" in newbody:
                self.logger.debug("Strong tags found. Replacing them.")
                newbody = newbody.replace("<strong>", "")
                newbody = newbody.replace("</strong>", "")

            newbody += "\n\n^____________________________\n\n**" \
                       "Comment !goal to create a link to a goal in the Match Events section of this thread. " \
                       "For example \"!goal 6 <link>\" or \"!goal 3-3 <link>\". " \
                       "Any comment not downvoted will be picked up.**\n\n--------" \
                       "\n\n*^(Don't see a thread for a match you're watching?) " \
                       "[^(Click here)](https://www.reddit.com/user/SFMatchThreadder/comments/l48zcl/) " \
                       "^(to learn how to request a match thread from this bot.)*"

            # save data
            if newbody != body:
                self.logger.info(f"Making edit to {team1} vs {team2} (/r/{sub})")
                try:
                    thread.edit(newbody)
                except Exception as exc:

                    print(self.reddit.auth.limits)
                    self.logger.error(f"Edit for {team1} vs {team2} failed. Likely a reddit server-side issue. "
                                      f"Exact error is: {type(exc)} ({exc.args}).")
                self.save_active_threads()
            else:
                self.logger.info(f"No change in the {team1} vs {team2} thread. (/r/{sub})")

            newdata = match_id, team1, team2, thread_id, reqr, sub, thread_type
            self.activeThreads[index] = newdata

            if finished:
                # todo wait like 5 times before deleting?
                toRemove.append(newdata)

        for getRid in toRemove:
            match_id, team1, team2, og_thread_id, reqr, sub, thread_type = getRid

            if match_id in self.post_match_attempts.keys():
                count = self.post_match_attempts[match_id]
                count += 1
                self.post_match_attempts[match_id] = count
            else:
                self.post_match_attempts[match_id] = 1
            code, thread_id = self.submit_postmatch_thread(getRid, bbc_link=True)

            if self.post_match_attempts[match_id] >= 5 or code is ExceptionCodes.SUCCESS:
                if code is ExceptionCodes.FAILURE:
                    # made 5 attempts, just post the body of the thread and move the links to the top
                    code, thread_id = self.submit_postmatch_thread(getRid, bbc_link=False)

                if code is ExceptionCodes.SUCCESS:
                    thread = self.reddit.submission(id=og_thread_id)
                    body = thread.selftext
                    body = f"#This match is now over. Join the post match discussion [here](https://www.reddit.com/r/{sub}/comments/{thread_id})\n" + body
                    thread.edit(body)

                # either made 5 attempts or was a success, stop tracking anyway
                self.activeThreads.remove(getRid)
                self.logger.info(f"Active threads: {len(self.activeThreads)} "
                                 f"- removed {getRid[1]} vs {getRid[2]} (/r/{getRid[5]})")
                self.save_active_threads()

    # helper method, will return whether the team is about to play
    def is_game_soon(self, team, tracked_dict):
        added = False
        match_id = self.scraper.findMatchSiteExact(team.lower())
        if match_id != ExceptionCodes.NO_MATCH:
            # only some things matter rn
            if match_id not in self.match_info.keys():
                self.match_info[match_id] = self.scraper.getMatchInfo(match_id)

            team1fix, _, team2fix, *_, comp, _, _ = self.match_info[match_id]
            self.logger.debug(f"Information recieved about {team}'s game: "
                              f"matchID - {match_id}. \nGeneral info: {self.match_info[match_id]}")

            *_, ko_time, status, _, _, _ = self.match_info[match_id]

            hour_i, min_i = self.scraper.getTimes(ko_time)
            now = datetime.datetime.now()

            # todo, handle games around midnight
            game_time = datetime.datetime(now.year, now.month, now.day, hour_i, min_i, 0)

            allowed_time = now+datetime.timedelta(hours=1, minutes=30)

            # is the game in the next hour?
            if game_time <= allowed_time:
                # is the game finished?
                if not (status.startswith('FT') or status == 'AET' or status == 'Postponed'):
                    # store team and match id
                    tracked_dict[team1fix] = match_id
                    tracked_dict[team2fix] = match_id
                    added = True
            else:
                self.logger.debug(f"{team} aren't playing till {game_time}, {allowed_time} is the soonest "
                                  f"available time to track.")

        return added, match_id

    def post_big_games(self, match_id):
        post = False
        team_a, _, team_b, *_, ko_time, status, comp, t1abb, t2abb = self.match_info[match_id]

        derby_games = [("Rangers", "Celtic"), ("Hearts", "Hibs"), ("Dundee", "Dundee United")]
        for derby in derby_games:
            if (team_a, team_b) == derby or (team_b, team_a) == derby:
                post = True

        if "Scotland" in [team_a, team_b]:
            post = True

        if "Final" in comp:
            post = True

        if post:
            hour_i, min_i, now = self.scraper.getTimes(ko_time)
            track_game_time = now + datetime.timedelta(hours=1)
            # is the game less than 1 hour way
            if (track_game_time.hour > hour_i) or (
                    (track_game_time.hour == hour_i) and (track_game_time.minute > min_i)):
                status, thread_id = self.createNewThread(team_a, "", "mf__4", self.default_subreddit, match_id)
                return status, thread_id, True

        return "", "", False

    # r/scottishfootball games
    def check_default_sub_games(self):
        self.scottishfootball_attempts += 1
        self.logger.info(f"r/{self.default_subreddit} - COUNT IS {self.scottishfootball_attempts}")

        # All teams to consider
        # todo auto pull this
        all_teams = ["Rangers", "Celtic", "Hibernian", "Aberdeen",
                     "Heart of Midlothian", "Dundee United", "Ross County", "Livingston",
                     "St Johnstone", "Motherwell", "St Mirren", "Kilmarnock",
                     "Scotland"]

        all_teams_constant = all_teams.copy()

        active_teams = []
        # todo do this every loop? why not only when about to check
        # dont check for any team who's already in a thread
        for thread in self.activeThreads:
            _, t1, t2, _, _, sub, thread_type = thread
            # only care about scottishfootball threads
            if sub == self.default_subreddit:
                self.logger.debug(f"r/{self.default_subreddit} - FOUND ACTIVE THREAD - {t1} vs {t2}")

                if t1 in all_teams:
                    active_teams.append(t1)
                    all_teams.remove(t1)
                if t2 in all_teams:
                    active_teams.append(t2)
                    all_teams.remove(t2)

                if t1 in self.scottishfootball_tracked_teams.keys():
                    del self.scottishfootball_tracked_teams[t1]
                if t2 in self.scottishfootball_tracked_teams.keys():
                    del self.scottishfootball_tracked_teams[t2]

                self.logger.debug(f"r/{self.default_subreddit} - TEAMS {t1} & {t2} NO LONGER CONSIDERED")

        # see if any games are less than an hour away roughly every 30 mins
        if self.scottishfootball_attempts >= 30:
            self.scottishfootball_attempts = 0

            self.logger.info("Checking for new games...")
            self.logger.debug(f"r/{self.default_subreddit} - CONSIDERED TEAMS: {all_teams}")
            for team in sorted(all_teams):
                if team not in active_teams and team not in self.scottishfootball_tracked_teams:
                    added, match_id = self.is_game_soon(team, self.scottishfootball_tracked_teams)
                    if added:
                        team1fix, _, team2fix, *_, comp, _, _ = self.match_info[match_id]
                        message = f"{team1fix} vs {team2fix} is less than an hour away for competition: {comp}. " \
                                  f"(http://www.espn.com/soccer/match?gameId={match_id})"
                        self.telegram_bot.send_owner_message(message)

        self.logger.info(f"r/{self.default_subreddit} - TEAMS TO PLAY IN NEXT HOUR: {self.scottishfootball_tracked_teams}")

        to_remove = []
        other_team = []
        for team in self.scottishfootball_tracked_teams:
            if team in other_team:
                # was this game already posted?
                continue

            match_id = self.scottishfootball_tracked_teams[team]
            status, thread_id, posted = self.post_big_games(match_id)
            if not posted:
                status, thread_id = self.createNewThread(team, "",  self.bot_username, self.default_subreddit, match_id)

            team_1, _, team_2, *_, comp, _, _ = self.match_info[match_id]

            if status == ThreadCodes.SUCCESS:
                self.logger.debug(f"variables received are: team1: {team_1}, team2: {team_2}, comp: {comp}")

                message = f"New Match Thread made for {team_1} vs {team_2} who are playing in the {comp}. " + \
                          f"(Link to thread: http://www.reddit.com/r/{self.default_subreddit}/comments/{thread_id})"

                self.telegram_bot.send_owner_message(message)

                # dont attempt to post teams twice, do this after incase it crashes when trying to send that message
                other_team.append(team_1)
                other_team.append(team_2)

                to_remove.append(team)
                to_remove.append(team_1)
                to_remove.append(team_2)

            elif status == ThreadCodes.GAME_OVER:
                to_remove.append(team)
                to_remove.append(team_1)
                to_remove.append(team_2)
                message = f"Match for {team_1} vs {team_2} is finished? Its either FT AET or Postponed. " \
                          f"ESPN Link is http://www.espn.com/soccer/match?gameId={match_id}"

                self.telegram_bot.send_owner_message(message)
                self.logger.warning(message)

        for added in to_remove:
            if added in self.scottishfootball_tracked_teams.keys():
                self.logger.debug(f"Removing {added} from tracked list.")
                del self.scottishfootball_tracked_teams[added]


    # custom subreddit games (eg rangers games for r/rangersfc, celtic games for r/celticfc)
    def check_sub_games(self):
        self.customsubs_attempts += 1
        self.logger.debug(f"custom subreddits - COUNT is {self.customsubs_attempts}")

        relevant_subreddit = self.customsubs.copy()

        # relevant_subreddit = {"Rangers": 'SFMatchThreads'}
        # "Celtic": u'celticfc',
        # "Hibernian": u'hibsfc',
        # "Aberdeen": u'aberdeenfc',
        # "Kilmarnock": u'killie',
        # "Dundee United": u'dundeeunited',
        # "St Johnstone": u'stjohnstone',
        # "Motherwell": u'motherwellfc'}

        active_teams = []
        # dont check for any team who's already in a thread
        for thread in self.activeThreads:
            _, t1, t2, _, _, sub, thread_type = thread

            if sub != self.default_subreddit:
                self.logger.debug(f"{sub} - FOUND ACTIVE THREAD - {t1} vs {t2}")

            # is the active thread in the personal subreddit for each team?
            if t1 in relevant_subreddit.keys():
                if relevant_subreddit[t1].lower() == sub.lower():
                    active_teams.append(t1)
                    del relevant_subreddit[t1]
                    if t1 in self.custom_subs_tracked_teams.keys():
                        del self.custom_subs_tracked_teams[t1]

                    self.logger.debug(f"{sub} - TEAM {t1} NO LONGER CONSIDERED")

            if t2 in relevant_subreddit.keys():
                if relevant_subreddit[t2].lower() == sub.lower():
                    active_teams.append(t2)
                    del relevant_subreddit[t2]
                    if t2 in self.custom_subs_tracked_teams.keys():
                        del self.custom_subs_tracked_teams[t2]

                    self.logger.debug(f"{sub} - TEAM {t2} NO LONGER CONSIDERED")


        # see if any games are less than an hour away roughly every 30 mins
        if self.customsubs_attempts >= 30:
            self.customsubs_attempts = 0

            self.logger.debug("Checking for new games...")
            self.logger.debug(f"custom subreddits - CONSIDERED TEAMS: {relevant_subreddit}")
            for team in sorted(relevant_subreddit.keys()):
                if team not in active_teams and team not in self.custom_subs_tracked_teams:
                    added, match_id = self.is_game_soon(team, self.custom_subs_tracked_teams)
                    if added:
                        team1fix, _, team2fix, *_, comp, _, _ = self.match_info[match_id]
                        message = f"{team1fix} vs {team2fix} is less than an hour away for competition: {comp}. " \
                                  f"(http://www.espn.com/soccer/match?gameId={match_id})." \
                                  f"This is for r/{relevant_subreddit[team]}"
                        self.telegram_bot.send_owner_message(message)


        self.logger.info(f"custom subreddits - TEAMS TO PLAY IN NEXT HOUR: {self.custom_subs_tracked_teams}")

        to_remove = []
        for team in self.custom_subs_tracked_teams:
            match_id = self.custom_subs_tracked_teams[team]
            t1, _, t2, *_ = self.match_info[match_id]
            subreddit = ""
            if team in relevant_subreddit.keys():
                subreddit = relevant_subreddit[team]
            else:
                if t1 == team:
                    subreddit = relevant_subreddit[t2]
                elif t2 == team:
                    subreddit = relevant_subreddit[t1]
            status, thread_id = self.createNewThread(team, "",  self.bot_username, subreddit, match_id)

            if status == ThreadCodes.SUCCESS:
                to_remove.append(team)
                to_remove.append(t1)
                to_remove.append(t2)

                message = f"New Match Thread made for {team}" + \
                          f"(Link to thread: http://www.reddit.com/r/{relevant_subreddit[team]}/comments/{thread_id})"

                self.telegram_bot.send_owner_message(message)

            elif status == ThreadCodes.GAME_OVER:
                to_remove.append(team)
                to_remove.append(t1)
                to_remove.append(t2)
                message = f"Match for {team} is finished? Its either FT AET or Postponed. " \
                          f"ESPN Link is http://www.espn.com/soccer/match?gameId={match_id}"

                self.telegram_bot.send_owner_message(message)
                self.logger.warning(message)

        for added in to_remove:
            if added in self.custom_subs_tracked_teams.keys():
                self.logger.debug(f"Removing {added} from tracked list.")
                del self.custom_subs_tracked_teams[added]

    # post any game thats on bbc
    def getBBCGames(self):

        self.bbcgames_attempts += 1
        self.logger.info(f"BBC Games - COUNT IS {self.bbcgames_attempts}")

        if self.bbcgames_attempts >= 30:
            self.bbcgames_attempts = 0
            teams = self.scraper.getBBCGames()
            if teams is None:
                self.logger.debug("No matches found on BBC")
                return
            teams = re.findall("(.+) vs? (.+)", teams)[0]
            active_teams = []
            # dont check for any team who's already in a thread
            for thread in self.activeThreads:
                _, t1, t2, _, _, sub, thread_type = thread
                team_a, team_b = teams
                # only care about scottishfootball threads
                if t1 in [team_a, team_b] and sub == self.default_subreddit:
                    return  # thread already exists
                self.logger.debug(f"BBC Games - Thread already exists for {team_a} vs {team_b}")

            # see if any games are less than an hour away roughly every 30 mins


            self.logger.info("Checking for new games...")
            self.logger.info(f"BBC Games - CONSIDERED TEAMS: {teams}")
            for team in sorted(teams):
                if team not in active_teams and team not in self.bbcgames_tracked_teams:
                    added, match_id = self.is_game_soon(team, self.bbcgames_tracked_teams)
                    if added:
                        team1fix, _, team2fix, *_, comp, _, _ = self.match_info[match_id]
                        message = f"{team1fix} vs {team2fix} is less than an hour away for competition: {comp}. " \
                                  f"(http://www.espn.com/soccer/match?gameId={match_id})"
                        self.telegram_bot.send_owner_message(message)

        self.logger.info(f"BBC Games - TEAMS TO PLAY IN NEXT HOUR: {self.bbcgames_tracked_teams}")

        to_remove = []
        other_team = []
        for team in self.bbcgames_tracked_teams:
            if team in other_team:
                # was this game already posted?
                continue

            match_id = self.bbcgames_tracked_teams[team]
            status, thread_id = self.createNewThread(team, "", self.bot_username, self.default_subreddit, match_id)

            team_1, _, team_2, *_, comp, _, _ = self.match_info[match_id]

            if status == ThreadCodes.SUCCESS:
                self.logger.debug(f"variables received are: team1: {team_1}, team2: {team_2}, comp: {comp}")

                message = f"New Match Thread made for game on BBC. {team_1} vs {team_2} are playing in the {comp}. " + \
                          f"(Link to thread: http://www.reddit.com/r/{self.default_subreddit}/comments/{thread_id})"

                self.telegram_bot.send_owner_message(message)

                # dont attempt to post teams twice, do this after incase it crashes when trying to send that message
                other_team.append(team_1)
                other_team.append(team_2)

                to_remove.append(team)
                to_remove.append(team_1)
                to_remove.append(team_2)

            elif status == ThreadCodes.GAME_OVER:
                to_remove.append(team)
                to_remove.append(team_1)
                to_remove.append(team_2)
                message = f"Match for {team_1} vs {team_2} is finished? Its either FT AET or Postponed. " \
                          f"ESPN Link is http://www.espn.com/soccer/match?gameId={match_id}"

                self.telegram_bot.send_owner_message(message)
                self.logger.warning(message)

        for added in to_remove:
            if added in self.bbcgames_tracked_teams.keys():
                self.logger.debug(f"Removing {added} from tracked list.")
                del self.bbcgames_tracked_teams[added]

    def submit_postmatch_thread(self, data, bbc_link=True):
        match_id, team1, team2, thread_id, reqr, sub, thread_type = data
        self.telegram_bot.send_owner_message(f"Attempting to post postmatch thread for {team1} vs {team2}")
        if bbc_link:
            link = None
            # attempt to get the BBC page
            code, bbc_id, t1, t2 = self.scraper.findBBCSiteSingle(team1, team2)
            if code != ExceptionCodes.NO_MATCH:
                request, bbc_url = self.scraper.get_BBC_page(bbc_id)
                if request.status_code == 200 and "https://www.bbc.co.uk/sport/football" in bbc_url:
                    # checking its valid and that its not a live page
                    link = bbc_url

            if link is None:
                self.telegram_bot.send_owner_message(f"{team1} vs {team2} does not have a completed BBC page yet, will wait till one exists")
                self.logger.info(f"{team1} vs {team2} does not have a completed BBC page yet, will wait till one exists")
                return ExceptionCodes.FAILURE, ""
        else:
            # cant find BBC page, just post thread
            thread = self.reddit.submission(id=thread_id)
            body = thread.selftext
            body = f"##Couldn't find a BBC page quick enough, copy of [Match Thread](https://www.reddit.com/r/{sub}/comments/{thread_id}) text below.\n" \
                   f"###[ESPN Link](http://www.espn.com/soccer/match?gameId={match_id})\n\n" \
                   f"{body}"

        team1_goals, team2_goals = self.scraper.get_score(match_id=match_id)
        title = f"{team1} {team1_goals}-{team2_goals} {team2}"

        submitted = False

        # try assign a match thread flair if possible
        if self.reddit.subreddit(sub).can_assign_link_flair:
            self.logger.debug(f"/r/{sub} allows flairs to be allocated.")

            flairs = self.reddit.subreddit(sub).flair.link_templates
            flair_texts = ""
            for f in flairs:
                flair_texts += f"{f['text']}, "

            flair_texts = flair_texts[:-2]
            self.logger.debug(f"Available flairs are {flair_texts}")

            # only select a flair which says match thread (dont worry about capitalisation)
            flair_id = -1
            for flair in flairs:
                if 'Match Report'.lower() == flair['text'].lower():
                    flair_id = flair['id']
                elif 'Results'.lower() == flair['text'].lower():
                    flair_id = flair['id']

            if flair_id != -1:
                submitted = True
                if bbc_link:
                    thread = self.reddit.subreddit(sub).submit(title,
                                                               url=link,
                                                               send_replies=False,
                                                               flair_id=flair_id)
                else:
                    thread = self.reddit.subreddit(sub).submit(title,
                                                               selftext=body,
                                                               send_replies=False,
                                                               flair_id=flair_id)

        if not submitted:
            if bbc_link:
                thread = self.reddit.subreddit(sub).submit(title, url=link, send_replies=False)
            else:
                thread = self.reddit.subreddit(sub).submit(title, selftext=body, send_replies=False)

        self.telegram_bot.send_owner_message(f"Posted post match thread for {title}. https://www.reddit.com/r/{sub}/comments/{thread}")

        return ExceptionCodes.SUCCESS, thread

    # get goal links from comments
    def getGoalLinks(self, thread, events):
        body = ""
        ## goals from comments
        thread.comments.replace_more(limit=None)
        comments = thread.comments.list()

        goals = {}
        comment_ref = {}
        for comment in comments:
            if comment.body.startswith("!goal"):
                comment_user = comment.author.name
                comment_score = comment.score
                self.logger.info(f"possible goal comment on thread {thread.title} by user {comment_user}. "
                                 f"comment: {comment.body} score: {comment_score}")

                if comment.score < 1 and comment_user not in usrwhitelist:
                    self.logger.debug(f"Comments score is too low ({comment_score}) and not user {comment_user} not "
                                      f"whitelisted. ignoring")
                    continue

                if comment_user in usrblacklist:
                    self.logger.debug(f"{comment_user} is in the blacklist ({usrblacklist}). ignoring their comment")
                    continue

                comment_body = comment.body
                if "<" in comment_body and ">" in comment_body:
                    comment_body = comment_body.replace("<", " ")
                    comment_body = comment_body.replace(">", " ")


                data_split = re.findall("!goal *(\d*)-?(\d*)? *([^ ]*)(?: .*)?", comment_body)

                if len(data_split[0]) == 3:
                    self.logger.debug("valid goal link found")
                    goal_num, other_goal, link = data_split[0]
                    if other_goal is not None:
                        # make sure the numbers are going to be added correctly
                        if type(goal_num) is str and type(other_goal) is str:
                            try:
                                goal_num = int(goal_num)
                                other_goal = int(other_goal)
                            except Exception:
                                self.logger.error(f"Failed to parse {goal_num} and {other_goal} to ints.")

                        # get goal number
                        goal_num = goal_num+other_goal
                        self.logger.debug(f"Comment {comment_body} is about goal {goal_num}?")

                    # was it submitted as a reddit link? if so parse out the actual link part
                    extract = re.findall("\[.*\]\((.*)\)", link)
                    if len(extract) > 0:
                        link = extract[0]


                    # check which comment to go off of
                    """
                    1. Admin
                    2. In whitelist
                    3. Score
                    4. First seen
                    """

                    # upvote any comment that was correct
                    comment.upvote()
                    try:
                        num = int(goal_num)
                        if num in comment_ref.keys():
                            other_comment_user, other_comment_score = comment_ref[num]

                            # me
                            if comment_user == self.admin:
                                # comment found is by bot admin. choose it over all other links
                                self.logger.debug(f"User {comment_user} is the owner. prioritising this comment")
                                comment_ref[num] = comment_user, comment_score
                                goals[num] = link

                            # whitelist
                            elif comment_user in usrwhitelist and not other_comment_user == self.admin:
                                # comment found is by a user in the whitelist and other comment not made by owner

                                if other_comment_user in usrwhitelist:
                                    # both users are in the whitelist, pick most upvoted

                                    if comment_score > other_comment_score:
                                        self.logger.debug(f"User {comment_user} and {other_comment_user} are in the "
                                                          f"whitelist and {comment_user}'s comment is more "
                                                          f"upvoted ({comment_score} > {other_comment_score})")

                                        comment_ref[num] = comment_user, comment_score
                                        goals[num] = link
                                else:

                                    # other comment wasnt made by a whitelisted user, choose this one
                                    self.logger.debug(f"User {comment_user} is in the whitelist and "
                                                      f"{other_comment_user} is not. Prioritsing {comment_user}'s"
                                                      f" comment")
                                    comment_ref[num] = comment_user, comment_score
                                    goals[num] = link

                            # score
                            elif comment_score > other_comment_score and other_comment_user not in usrwhitelist \
                                    and not other_comment_user == self.admin:
                                self.logger.debug(f"Comment by {comment_user} is more upvoted than "
                                                  f"{other_comment_user}'s comment ({comment_score} > "
                                                  f"{other_comment_score})")
                                # only update if new comment is more upvoted is better
                                comment_ref[num] = comment_user, comment_score
                                goals[num] = link
                        else:
                            # first comment found for this goal
                            comment_ref[num] = comment_user, comment_score
                            goals[num] = link

                    except ValueError:
                        self.logger.warning(f"Comment {comment.body} was an attempt to link a goal but was invalid "
                                            f"(not a number).")
                    except Exception as e:
                        self.logger.warning(f"Comment {comment.body} was an attempt to link a goal but was invalid "
                                            f"({type(e)} {e.args}).")
                else:
                    self.logger.warning(f"Comment {comment.body} was an attempt to link a goal but was invalid "
                                        f"(link not found).")


        lines = events.split("\n")
        count = 0
        for line in lines:
            if "[](#icon-ball)" in line:
                timestamp, desc = line.split("[](#icon-ball)")
                count += 1
                if count in goals.keys():
                    link = goals[count]
                    formatted = "{} {} [{}]({})".format(timestamp.strip(), "[](#icon-ball)", desc.strip(), link)
                    body += formatted + "\n"
                else:
                    body += line + "\n"
            else:
                body += line + "\n"
        return body

    # general update loop
    def update_bot(self):
        self.check_and_create()
        self.check_default_sub_games()
        self.check_sub_games()
        self.getBBCGames()
        self.update_threads()

        # delete all unused info from the match_info dict
        # todo is this what i want to do? or should this be checking active threads, match info is cleared when thread is posted
        to_delete = []
        for match_id in self.match_info.keys():
            team_1, _, team_2, *_ = self.match_info[match_id]
            # ew, basically checking if either team is being tracked, only delete the info if they are
            if team_1 not in self.scottishfootball_tracked_teams and \
                    team_2 not in self.scottishfootball_tracked_teams and \
                    team_1 not in self.custom_subs_tracked_teams and \
                    team_2 not in self.custom_subs_tracked_teams and \
                    team_1 not in self.bbcgames_tracked_teams and \
                    team_2 not in self.bbcgames_tracked_teams:

                self.logger.debug(f"Removing {team_1} vs {team_2} from tracked match infos")
                to_delete.append(match_id)

        for match_id in to_delete:
            del self.match_info[match_id]